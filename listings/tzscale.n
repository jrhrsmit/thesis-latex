/*
-- File : tzscale.n
--
-- Contents : nML model for the tzscale processor.
--
-- Copyright (c) 2015-2019 Synopsys, Inc. This Synopsys processor model
-- captures an ASIP Designer Design Technique. The model and all associated
-- documentation are proprietary to Synopsys, Inc. and may only be used
-- pursuant to the terms and conditions of a written license agreement with
-- Synopsys, Inc.  All other use, reproduction, modification, or distribution
-- of the Synopsys processor model or the associated  documentation is
-- strictly prohibited.
*/

#include "tzscale_define.h"

enum stage_names {IF,   // Instruction Fetch
                  DE,   // Instruction Decode and Execute
                  WB};  // Writeback


// Program memory

def pm_size=2**14;

mem PMb[pm_size] <uint8,addr> access {};

mem PM[0..pm_size-4,2] <iword,addr> alias PMb align 2 access  {
    ifetch : pm_rd '1' = PM[pm_addr]'1';
#ifdef HAS_OCD
    istore : PM[pm_addr] = pm_wr;
#endif
};

properties {
    program_memory: PMb;
    unconnected : PM; // accessed in PCU
}

// Data memory

def dm_size=2**DM_SIZE_NBIT;

trn dmh_wr_hi<w08>;
trn dmw_wr_hi<w16>;

trn dmb_wr<w08>;
trn dmh_wr<w16> { dmb_wr; dmh_wr_hi; };
trn dmw_wr<w32> { dmh_wr; dmw_wr_hi; };

mem DMb [dm_size,1]<w08,addr> access {
    ld_dmb: dmb_rd '1' = DMb[dm_addr '0' ] '1';
    st_dmb: DMb[dm_addr] = dmb_wr;
};

mem DMh [dm_size-1,1]<w16,addr> alias DMb align 1 access {
    ld_dmh: dmh_rd '1' = DMh[dm_addr '0'] '1';
    st_dmh: DMh[dm_addr] = dmh_wr;
};

mem DMw [dm_size-3,1]<w32,addr> alias DMb align 1 access {
    ld_dmw: dmw_rd '1' = DMw[dm_addr '0'] '1';
    st_dmw: DMw[dm_addr] = dmw_wr;
};

// force stack alignment suitable for RV32C 16b instructions
#ifdef ISA_COMPRESSED
mem DM16x [2**(DM_SIZE_NBIT-4),1] <v16w08,addr> alias DMb access {};
property unconnected: DM16x;
#endif

// memory range that is directly addressable
mem DMb_stat [0..2047,1]<w08,addr> alias DMb[0] access {};
mem DMh_stat [0..2046,1]<w16,addr> alias DMb_stat align 1 access {};
mem DMw_stat [0..2044,1]<w32,addr> alias DMb_stat align 1 access {};

// Registers

enum eR { x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15
#ifndef RV32E
          , x16, x17, x18, x19, x20, x21, x22, x23, x24, x25, x26, x27, x28,
          x29, x30, x31
#endif
};

reg R[IF_RV32E(16,32)]<w32,uint5> syntax (eR) read(r1 r2) write(w1 wd);
#ifdef HWINITR
hw_init R = others => 0;
#else
#ifdef __checkers__
hw_init R = 0 => 0; // for ISS
#endif
#endif
//enum eABI { zero=0, ra, sp, gp, tp, t0, t1, t2, s0, s1,  a0,  a1,  a2,  a3,  a4,  a5,  a6,  a7,  s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9, s10, s11,  t3,  t4,  t5,  t6 };

reg R8_15[8]<w32,uint3> alias R[8];

reg zero <w32> alias R[0] read(r1 r2) write(); // =0

reg LR <w32> alias R[1] read() write(w1);  // link register

reg SP <w32> alias R[2]  read(r1) write(w1);  // stack pointer

reg PC <w32> read(pcr) write(pcw); hw_init PC = 0;  // program counter

properties {
    program_counter : PC;
    unconnected     : DMb_stat, DMh_stat, DMw_stat, LR, SP;
}

pipe PD<w32>; // pipe to designation register


reg rPC_DE<w32> read (tr_PC_DE) write(tw_PC_DE); // PM addr. of instr @ DE
hw_init rPC_DE = 0;

property unconnected : rPC_DE; // PCU

// Constants
cst c5u   <uint5>;
cst c5unz <uint5nz>;
cst c12u  <uint12>;
cst c12s  <int12>;
cst c13s2 <int13s2>;
cst c20p  <int20p>;
cst c21s2 <int21s2>;

// Properties

properties {
    endianness                  : little;
    program_memory_endianness   : little;
    decode_stage                : DE;
    chess_pc_offset             : words;
    control_signals             : compact;
}

// start of instruction set grammar


start tzscale;
opn tzscale (bit32_instr
#ifdef INCLUDE_RVC_RULES
            | bit16_instr
#endif
#ifdef HAS_OCD
    | swbrk_instr
#endif
#ifdef __programmers_view__
    | pseudo
#endif
);

// If RVC expansion is enabled, 16b instr not visible in CA ISS and HDL

opn bit32_instr(alu_instr | load_store_instr | control_instr | div_instr );

#ifdef HAS_OCD
#if defined(__go__)
opn always (ocd: ocd_if) { action { ocd; } }
#endif
#endif

#include "opcode.n"
#include "regfile.n"
#include "alu.n"
#include "ldst.n"
#include "control.n"
#include "div.n"
#include "pseudo.n"
#include "hazards.n"

#ifdef INCLUDE_RVC_RULES

opn bit16_instr_(c0 | c1 | c2);
opn bit16_instr(b16: bit16_instr_) { action: b16; syntax: b16; image: b16, class(bits16); }

#include "bits16.n"

#endif


#ifdef HAS_OCD
#include "ocd_if.n"
#endif
