PROJECT := thesis

MAIN := $(PROJECT).tex
SRCDIRS := chapters graphs tables

BIB=bib.bib
GLO=${PROJECT}.glo
CHAPTERS=$(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.tex))
FIGURES=$(wildcard figures/*.eps) 
FIGURES+=$(wildcard figures/*.pdf)
FIGURES+=$(wildcard figures/*.png)
SOURCES=$(CHAPTERS) $(MAIN) $(BIB) 
LATEST=$(shell ls -lt $(SOURCES) | head -n 6 | grep -oE "[^ ]*\..*")
OUT=outputs
TRASH=*.out *.gz *.db *.aux *.bbl *.log *.blg *.toc *.glo *.gls *.glg *.glsdefs *.ist _minted-$(PROJECT)

PDFLATEXFLAGS=--shell-escape -file-line-error -interaction=nonstopmode 
LLPP_PID=$(shell pgrep llpp)

$(PROJECT).pdf: $(SOURCES) $(FIGURES)
	mkdir -p $(OUT)
	mv -f $(OUT)/* . 2> /dev/null || true
	pdflatex $(PDFLATEXFLAGS) $(MAIN)
	bibtex $(PROJECT) | tee -a transcript.log
	makeglossaries $(GLO) | tee -a transcript.log
	pdflatex $(PDFLATEXFLAGS) $(MAIN)
	pdflatex $(PDFLATEXFLAGS) $(MAIN) | tee -a transcript.log
	if [ "$(LLPP_PID)" != "" ]; then pkill -HUP llpp; fi
	mv -f $(TRASH) $(OUT) 2> /dev/null || true
	rm -f tmp-image.pdf

edit:
	llpp $(PROJECT).pdf &
	vim $(LATEST)

clean:
	rm -rf $(TRASH) ${OUT} $(PROJECT).pdf
	rm -f tmp-image.pdf
	find . -iname "*-eps-converted-to.pdf" -delete
