# LaTeX source repo for thesis
## Dependencies
Dependencies on Archlinux:
 - `texlive-core`
 - `texlive-extra`
 - `texlive-ieeetran`
 - `texlive-science`
 - `ghostscript` (for `minted`)
 - `minted`
 - `make`
 - `which`

## Compilation
The compilation of the pdf is done with a Makefile, so just run:
`make`
and the output will be written to `thesis.pdf`.

## TODO
    - Impbench expansion (NNs, fix MLZO's DMEM usage)
    - Ask Rene vd Leuken and Ali about NNs (is CNN a possibility?)
    - Measurements:
        - Power at same ex. time
    - Discussions:
        - Correlations between scores ImpBench and CoreMark
        - Impact of memory on the power consumption
        - Impact of power measurement using embedded multipliers (alternatives?)
    - If time permits:
        - Implementing/adding the NN to ImpBench
