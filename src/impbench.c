#include <stdio.h>

#include "crc32.h"
#include "csum.h"
#include "data_gen.h"
#include "fin.h"
#include "misty1.h"
#include "motion.h"
#include "print.h"
#include "rc6.h"
#include "coremark.h"

#ifdef RUN_BENCH
    #if RUN_BENCH == 0
        #define BENCH crc
    #else
        #if RUN_BENCH == 1
            #define BENCH csum
        #else
            #if RUN_BENCH == 2
                #define BENCH misty1
            #else
                #if RUN_BENCH == 3
                    #define BENCH motion
                #else
                    #if RUN_BENCH == 4
                        #define BENCH rc6
                    #else
                        #if RUN_BENCH == 5
                            #define BENCH fin
                        #else
                            #if RUN_BENCH == 6
                                #define BENCH core_main
                            #endif
                        #endif
                    #endif
                #endif
            #endif
        #endif
    #endif
    #ifndef BENCH
        #error "RUN_BENCH out of range 0..6"
    #endif
#endif

/**
 * Run a benchmark
 *
 * Run a benchmark from the ImpBench suite.
 * First the bench number is printed to stdout. If the benchmark succeeds and
 * uses the original ImpBench data (i.e. DATA_GEN_RANDOM is set to 0), 'V' is
 * printed to stdout, if it fails 'X' is printed. If random data is used, lower 
 * case is used.
 * The benchmarks 'fail' when their checksum is not correct.
 * This also means that if an 'x' is printed, it doesn't necessarily mean that
 * the benchmark has failed, because random data was used.
 *
 * Benchmark numbers and names:
 * 0    CRC32
 * 1    CSUM
 * 2    MISTY1
 * 3    MOTION
 * 4    RC6
 * 5    FIN
 *
 * Meaning of printed character
 *                      DATA_GEN_RANDOM=0   DATA_GEN_RANDOM=1
 * Bench returns 0      V                   v
 * Bench returns != 0   X                   x
 *
 * @p num the benchmark number
 */
static void run_benchmark(int num) {
    volatile uint32_t res = 0;
    int32_t (*fn)(void);
#ifndef RUN_BENCH
    int32_t (*bench_fn[])(void) = {crc, csum, misty1, motion, rc6, fin, core_main};
    fn = bench_fn[num];
#else
    fn = BENCH;
#endif
    write_stdout('0' + num);
    res = fn();
    if (res) {
        write_stdout(DATA_GEN_RANDOM ? 'x' : 'X');
    } else {
        write_stdout(DATA_GEN_RANDOM ? 'v' : 'V');
    }
}

int main(void) {
    do {
#ifndef RUN_BENCH
        for(int i=0; i<7; i++) {
            run_benchmark(i);
        }
#else
        run_benchmark(RUN_BENCH);
#endif
    } while(LOOP);
    return 0;
}
