iss::create %PROCESSORNAME% iss
puts $argv
set n [expr $argc -3]
set prj [lindex $argv $n]
if {$prj == "tzscale"} {
    set dm "DMb"
} elseif {$prj == "tdsp"} {
    set dm "DMw"
} else {
    set dm "DM"
}
set n [expr $argc -1]
set hits [lindex $argv $n]
puts "Project: $prj DM: $dm hits: $hits"
iss program load ./Release/impbench -disassemble -dwarf -nmlpath /home/jrhrsmit/impbench_tce/thesis-asip-projects/$prj/lib -extradisassembleopts +Mdec -do_not_set_entry_pc 1 -do_not_load_sp 1 -pm_check first -load_offsets {} -software_breakpoints_allowed on -hardware_breakpoints_allowed on
iss watchpoint add $dm 4 -write true
set fp [open "sim_output" w]
for {set i 0} {$i < $hits} {incr i} {
    iss step -1
    set x [iss get $dm 4]
    set x [format %c $x]
    puts -nonewline $fp $x
}
iss close
exit
