iss::create %PROCESSORNAME% iss
iss program load /home/jrhrsmit/tzscale/impbench_mod/Release/impbench -disassemble -dwarf -nmlpath /home/jrhrsmit/tzscale/lib -extradisassembleopts +Mdec -do_not_set_entry_pc 1 -do_not_load_sp 1 -pm_check first -load_offsets {} -software_breakpoints_allowed on -hardware_breakpoints_allowed on
set fp [open "pm_rd_trace" w]
for {set i 1} {$i <= 1000000} {incr i} {
    iss step 1
    set x [iss get pm_rd]
    set x [format %x $x]
    set line "$i : $x"
    puts $fp $line
}
close $fp
iss close
exit
