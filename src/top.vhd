library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.data_types.all;

entity top is
    port (
             clock_50mhz_in : in std_logic;
             areset : in std_logic;
             pm_rd_out: out unsigned(15 downto 0);
             gnd: out std_logic_vector(17 downto 0)
         );
end top;

architecture behavioural of top is

    component altera_jtag_uart_stdout_always_1 is
        generic (
                    dataw : integer := 8);
        port (
                 t1data   : in  std_logic_vector(dataw-1 downto 0);
                 t1load   : in  std_logic;
                 clk      : in  std_logic;
                 rstx     : in  std_logic;
                 glock    : in  std_logic;
                 lock_req : out std_logic
             );
    end component;

    component clock_gen
        port (
                 reset_out : out std_logic;
                 clock_out : out std_logic);
    end component;

    component top_mem_pmb 
        port (
                 reset : in std_logic;
                 clock : in std_logic;
                 ifetch_in : in std_logic;
                 pm_addr_in : in t_addr;
                 pm_rd_out : out t_iword);
    end component;

    component top_mem_dm0 
        port (
                 reset : in std_logic;
                 clock : in std_logic;
                 ld_dm0_in : in t_uint1_t;
                 dm0_addr_in : in t_uint20_t;
                 dm0_rd_out : out t_v4uint8;
                 st_dm0_in : in t_v4uint1;
                 dm0_wr_in : in t_v4uint8);
    end component;

    component top_mem_dm1 
        port (
                 reset : in std_logic;
                 clock : in std_logic;
                 ld_dm1_in : in t_uint1_t;
                 dm1_addr_in : in t_uint20_t;
                 dm1_rd_out : out t_v4uint8;
                 st_dm1_in : in t_v4uint1;
                 dm1_wr_in : in t_v4uint8);
    end component;

    component tzscale
        port (
                 clock : in std_logic;
                 reset : in std_logic;
                 pm_rd_in : in t_iword;
                 pm_addr_out : out t_addr;
                 ifetch_out : out std_logic;
                 ld_dm0_out : out t_uint1_t;
                 dm0_addr_out : out t_uint20_t;
                 dm0_rd_in : in t_v4uint8;
                 st_dm0_out : out t_v4uint1;
                 dm0_wr_out : out t_v4uint8;
                 ld_dm1_out : out t_uint1_t;
                 dm1_addr_out : out t_uint20_t;
                 dm1_rd_in : in t_v4uint8;
                 st_dm1_out : out t_v4uint1;
                 dm1_wr_out : out t_v4uint8);
    end component;

    component pll
        port (
                 areset : in std_logic := '0';
                 inclk0 : in std_logic := '0';
                 c0 : out std_logic;
                 locked : out std_logic
             );
    end component;

    signal clock : std_logic;
    signal reset : std_logic;
    signal pm_rd : t_iword;
    signal pm_addr : t_addr;
    signal pm_wr : t_iword;
    signal istore : std_logic;
    signal ifetch : std_logic;
    signal ld_dm0 : t_uint1_t;
    signal dm0_addr : t_uint20_t;
    signal dm0_rd : t_v4uint8;
    signal st_dm0 : t_v4uint1;
    signal dm0_wr : t_v4uint8;
    signal ld_dm1 : t_uint1_t;
    signal dm1_addr : t_uint20_t;
    signal dm1_rd : t_v4uint8;
    signal st_dm1 : t_v4uint1;
    signal dm1_wr : t_v4uint8;
    signal pll_locked : std_logic;
    signal nareset : std_logic;
    signal uart_data : std_logic_vector(7 downto 0);
    signal uart_data_new : std_logic;

begin

    nareset <= not areset;
    pm_rd_out <= (others => '0');
    gnd <= (others => '0');

    process (clock, areset)
    begin
        if (rising_edge(clock)) then
            if (nareset = '1' or pll_locked = '0') then
                reset <= '1';
            else
                reset <= '0';
            end if;
        end if;
    end process;

    inst_pll : pll
    port map
    (
        areset => nareset,
        inclk0 => clock_50mhz_in,
        c0 => clock,
        locked => pll_locked
    );

    process(clock)
        constant data_addr : t_addr := x"00000000";
    begin
        if(rising_edge(clock)) then
            if(nareset = '0') then
                if(st_dm1 = x"F" and dm1_addr = x"0000") then
                    uart_data_new <= '1';
                    uart_data <= std_logic_vector(dm1_wr(7 downto 0));
                else
                    uart_data_new <= '0';
                end if;
            else
                uart_data_new <= '0';
                uart_data <= (others => '0');
            end if;
        end if;
    end process;

    inst_uart : altera_jtag_uart_stdout_always_1
    port map (
                 t1data   => uart_data,
                 t1load   => uart_data_new,
                 clk      => clock,
                 rstx     => areset,
                 glock    => '0');

    inst_top_mem_pmb : top_mem_pmb
    port map (
                 reset => reset,
                 clock => clock,
                 ifetch_in => ifetch,
                 pm_addr_in => pm_addr,
                 pm_rd_out => pm_rd);

    inst_top_mem_dm0 : top_mem_dm0
    port map (
                 reset => reset,
                 clock => clock,
                 ld_dm0_in => ld_dm0,
                 dm0_addr_in => dm0_addr,
                 dm0_rd_out => dm0_rd,
                 st_dm0_in => st_dm0,
                 dm0_wr_in => dm0_wr);

    inst_top_mem_dm1 : top_mem_dm1
    port map (
                 reset => reset,
                 clock => clock,
                 ld_dm1_in => ld_dm1,
                 dm1_addr_in => dm1_addr,
                 dm1_rd_out => dm1_rd,
                 st_dm1_in => st_dm1,
                 dm1_wr_in => dm1_wr);

    inst_tzscale : tzscale
    port map (
                 clock => clock,
                 reset => reset,
                 pm_rd_in => pm_rd,
                 pm_addr_out => pm_addr,
                 ifetch_out => ifetch,
                 ld_dm0_out => ld_dm0,
                 dm0_addr_out => dm0_addr,
                 dm0_rd_in => dm0_rd,
                 st_dm0_out => st_dm0,
                 dm0_wr_out => dm0_wr,
                 ld_dm1_out => ld_dm1,
                 dm1_addr_out => dm1_addr,
                 dm1_rd_in => dm1_rd,
                 st_dm1_out => st_dm1,
                 dm1_wr_out => dm1_wr);

end behavioural;

