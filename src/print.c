#include "print.h"
#include <stdint.h>
#include <stdio.h>

/**
 * @file print.c
 * @author Jasper Smit
 * 
 * A source file containing substitutes for simple calls to printf()
 */

void write_stdout(char c) {
#ifdef __chess__
    int volatile * const p = (int *) 0x0000004;
    *p = c;
#else
    putchar(c);
#endif
}

/** Prints a string
 *
 * @p s a pointer to a null-terminated string
 */
void print_str(const char* s) {
    while (*s != '\0')
        write_stdout(*(s++));
}

/** Prints an int in decimal
 *
 * @p i the 32-bit signed int to be printed
 */
void print_int(int32_t i) {
    int j = 8, k, negative = 0;
    // no dynamic allocation, maximum of 10 digits (can fit 2^31 in dec)
    char str[10] = "         ";
    if (i < 0) {
        negative = 1;
        i = -i;
    }
    while (i) {
        // start at the least significant digit, set the last character of the
        // string to the modulo of 10 (i.e. last decimal digit) and add the
        // ASCII '0' to convert it to an ASCII character.
        str[j--] = i % 10 + '0';
        // remove the last decimal digit for the next iteration
        i = i / 10;
    }
    if (negative)
        write_stdout('-');
    // print the newly generated string
    for (k = j + 1; k < 9; k++)
        write_stdout(str[k]);
}

/** Prints an int in hexadecimal
 *
 * @p i the 32-bit unsigned int to be printed
 */
void print_hex(uint32_t i) {
    char c, str[8] = {0};
    int j, k;
    write_stdout('0');
    write_stdout('x');
    if (i == 0) {
        write_stdout('0');
        return;
    }
    for (j = 7; j >= 0 && i; j--) {
        c = i & 0x0F;
        if (c > 9)
            c += 'A' - 10;
        else
            c += '0';
        str[j] = c;
        i >>= 4;
    }
    for (k = j + 1; k < 8; k++)
        write_stdout(str[k]);
}
