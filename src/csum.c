/*  + Name:
 *  ImpBench/checksum
 *
 *  + Details:
 *  This program is a modified version of the "cksum.c" source. It calculates
 * the chechsum of any ascii or binary file. It reads the whole file into a
 * buffer, calculates the total checksum, copies the whole file to a separate
 * output file and appends the calculated checksum to the end of the same file.
 *  This copy of data is actually done to avoid writing the input file. Writing
 * to the output can be easily disabled and the checksum results returned by the
 * main function.
 *
 *  + Syntax:
 *  <exec> <in filename> <out filename>
 *
 *  + Copyright:
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis and Christofer Kachris, CE Lab, TU Delft, 2008. All rights
 * reserved (C).
 */

#include "csum.h"
#include <stdint.h>
#include <string.h>
#include "data_gen.h"

#include "print.h"

// Compute checksum for "count" bytes beginning at location "addr".
static uint32_t checksum(uint32_t sum, uint32_t data[], int len) {
    int i;
    for(i = 0; i < len / 4; i++) {
        sum += ~(data[i] & 0xFFFF);
        sum += ~(data[i] >> 16);
    }
    // add left-over 16-bits, if any
    if(len % 4 >= 2) {
        sum += ~(data[i] & 0xFFFF);
        data[i] >>= 16;
    }
    // add left-over byte, if any
    if(len & 1) {
        // don't negate this one (stay true to impbench src)
        sum += data[i];
    }
    return sum;
}

int32_t csum(void) {
    uint32_t i;
    uint32_t sum = 0;
    uint8_t data[CSUM_BLK_SIZE];
    data_gen_reset();
    for (i = 0; i < CSUM_DATA_SIZE - CSUM_BLK_SIZE + 1; i += CSUM_BLK_SIZE) {
        data_gen_buf((uint32_t*)data, CSUM_BLK_SIZE);
        sum = checksum(sum, (uint32_t*)data, CSUM_BLK_SIZE);
    }
    if (CSUM_DATA_SIZE - i > 0) {
        data_gen_buf((uint32_t*)data, CSUM_DATA_SIZE - i);
        sum = checksum(sum, (uint32_t*)data, CSUM_DATA_SIZE - i);
    }
    // fold 32-bit sum to 16 bits
    while(sum & 0xFFFF0000) {
        sum = (sum & 0xffff) + (sum >> 16);
    }
    // verify result
    return sum != 13562;
}
