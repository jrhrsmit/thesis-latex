#include "endian.h"
#include <stdint.h>

/**
 * @file print.c
 * @author Jasper Smit
 * 
 * A source file containing only one function to be used to check endianness
 */

/** Check endianness
 *
 * @returns 1 if little endian, 0 if big endian
 */
int endian_is_little(void) {
    // set lower byte of a 16-bit int to 1
    uint16_t i = 1;
    // read out lower byte of i
    uint8_t* j = (uint8_t*)&i;
    // if the lower byte is 1, it is little endian
    return *j == 1;
}
