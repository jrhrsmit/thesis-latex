iss::create %PROCESSORNAME% iss
set n [incr argc -1]
set proc [lindex $argv $n]
# Enable profiling
iss profile set_active 1
iss profile storages_set_active 1
iss profile reset
iss profile storages_reset
# Load program
iss program load ./Release/impbench -disassemble -dwarf -nmlpath /home/jrhrsmit/impbench_tce/thesis-asip-projects/$proc/lib -extradisassembleopts +Mdec -do_not_set_entry_pc 1 -do_not_load_sp 1 -pm_check first -load_offsets {} -software_breakpoints_allowed on -hardware_breakpoints_allowed on
# Execute until finished
iss step -1
# Save profiling info
iss profile save profile_inst -type instruction_level
iss profile save profile_func -type function_level
iss profile storage_access_save -file profile_stor
iss close
exit

