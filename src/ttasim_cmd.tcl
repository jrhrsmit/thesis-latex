# Enable profiling
setting bus-trace 1
setting execution_trace 1
setting history_filename profile_hist
setting rf_tracking 1
setting utilization_data_saving 1
setting profile_data_saving 1
setting procedure_transfer_tracking 1
setting profile_data_saving 1
# Set machine and program
mach MACHINE
prog PROGRAM
run
info proc cycles
info proc stats
exit

