/*  + Name:
 *  ImpBench/Finnish
 *
 *  + Details:
 *  The code has been extensively commented for clarity, since no official
 *  documentation of the algorithm has been found. Further, the main() function
 *  has been rewritten more extensively.
 *
 *  + Syntax:
 *  <exec> <mode [c/d]> <in filename> <out filename>
 *
 *  + Copyright:
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2007. All rights reserved (C).
 *
 *  Original version written by Jussi Puttonen, 19.4.1991 at University of
 * Turku, Finland. Algorithms suggested by Timo Raita and Jukka Teuhola.
 *  Copyright 1991 by Jussi Puttonen, Timo Raita and Jukka Teuhola.
 *
 *  C-version written by N. de Vries.
 */

#include "fin.h"
#include "print.h"
#include <string.h>
#include <stdio.h>
#include "data_gen.h"

// table with predictions
#define PREDICTION_TABLE_SIZE 4096  // 32768U
char pcTable[PREDICTION_TABLE_SIZE] = {' '};

// macro to calculate index in pctable from previous 2 characters
//#define INDEX(p1, p2) (((unsigned)(unsigned char)p1 << 7) ^ (unsigned char)p2)
#define INDEX(p1, p2) \
    ((((unsigned)(unsigned char)p1 << 6) ^ (unsigned char)p2) & 0x0FFF)

static unsigned int Compress(uint32_t* a,
                             unsigned int a_len,
                             unsigned char* b,
                             unsigned int b_len) {
    char c;               // character
    int i;                // loop counter
    char p1 = 0, p2 = 0;  // previous 2 characters
    char buf[8];          // keeps characters temporarily
    int ctr = 0;          // number of characters in mask
    int bctr = 0;         // position in buf
    unsigned int j = 0, k = 0;
    unsigned char mask = 0;  // mask to mark successful predictions
    for (j = 0; j < a_len; j++) {
        c = a[j / 4] & 0xFF;
        a[j / 4] >>= 8;
        // try to predict the next character
        if (pcTable[INDEX(p1, p2)] == (char)c) {
            // correct prediction, mark bit for correct prediction
            mask = mask ^ (1 << ctr);
        } else {
            // wrong prediction, but next time ...
            pcTable[INDEX(p1, p2)] = (char)c;
            // buf keeps character temporarily in buffer
            buf[bctr++] = (char)c;
        }
        // test if mask is full (8 characters read)
        if (++ctr == 8) {
            // write mask
            b[k++] = mask;
            // write kept characters
            for (i = 0; i < bctr; i++) {
                if (k > b_len) {
                    print_str("out of bounds write on b\n");
                    break;
                }
                b[k++] = buf[i];
            }
            // reset variables
            ctr = 0;
            bctr = 0;
            mask = 0;
        }
        // shift characters
        p1 = p2;
        p2 = (char)c;
    }
    // EOF, but there might be some left for output
    if (ctr) {
        // write mask
        b[k++] = mask;
        // write kept characters
        for (i = 0; i < bctr; i++)
            b[k++] = buf[i];
    }
    return k;
}

static size_t Decompress(unsigned char* in,
                                                     unsigned int in_len,
                                                     unsigned char* out) {
    int ci, co;              // characters (in and out)
    char p1 = 0, p2 = 0;     // previous 2 characters
    int ctr = 8;             // number of characters processed for this mask
    unsigned char mask = 0;  // mask to mark successful predictions
    unsigned int i, j = 0;
    // TODO: out_len is not used for overflow detection
    // (void)out_len;

    memset(pcTable, 32,
           PREDICTION_TABLE_SIZE);  // space (ASCII 32) is the most used char

    for (i = 0; i < in_len;) {
        ci = in[i++];
        // get mask (for 8 characters)
        mask = ci;

        // for each bit in the mask
        for (ctr = 0; ctr < 8; ctr++) {
            if (mask & (1 << ctr)) {
                // predicted character
                co = pcTable[INDEX(p1, p2)];
            } else {
                // not predicted character
                co = in[i++];
                if (co == EOF)
                    return j;  // decompression completed !
                pcTable[INDEX(p1, p2)] = (char)co;
            }
            out[j++] = co;
            p1 = p2;
            p2 = co;
        }
    }
    return j;
}

/* test program by compressing and decompressing a file */
// compression test02: > ./benchmarks/fin/fin c ./files_input/AEP_10.asci
// ./files_output/test02.out
int32_t fin(void) {
    // unsigned char* a = fin_input_data;
    uint32_t w;
    uint32_t res = 0;
    int i;
    uint32_t data_in[FIN_BLK_SIZE];
    uint8_t data_out[FIN_BLK_SIZE];
    w = 0;
    data_gen_reset();
    // space (ASCII 32) is the most used char
    memset(pcTable, ' ', PREDICTION_TABLE_SIZE);
    for (i = 0; i < FIN_DATA_SIZE - FIN_BLK_SIZE + 1; i += FIN_BLK_SIZE) {
        data_gen_buf(data_in, FIN_BLK_SIZE);
        w += Compress(data_in, FIN_BLK_SIZE, data_out, FIN_BLK_SIZE);
        res += data_out[0];
    }
    if (i != FIN_DATA_SIZE) {
        data_gen_buf(data_in, FIN_DATA_SIZE - i);
        w += Compress(data_in, FIN_DATA_SIZE - i, data_out, FIN_BLK_SIZE);
        res += data_out[0];
    }
    return res != 0x00005748;
}
