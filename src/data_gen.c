#include "data_gen.h"

#include <stdint.h>
#include <string.h>

#include "endian.h"
#include "print.h"

/**
 * Variables used for generating impbench-like data
 * Used by data_gen_get8()
 * Gets reset by data_gen_reset()
 */
static uint16_t line_idx = 0, column_idx = 0;
static uint8_t data_mid_idx = 0, data_lines_idx = 0;

/**
 * Indicates endianness, gets set by data_gen_reset()
 */
static uint8_t little_endian = 1;

/** 
 * Unique portions of the data from AEP_10.ascii from ImpBench.
 * Used to generate the original ImpBench data in data_gen_get8()
 */
static const uint8_t data_mid[][4] = {
    {0x33, 0x30, 0x31, 0x34}, {0x32, 0x39, 0x38, 0x33},
    {0x32, 0x39, 0x30, 0x37}, {0x32, 0x38, 0x33, 0x31},
    {0x32, 0x37, 0x35, 0x34}, {0x32, 0x36, 0x37, 0x38},
    {0x32, 0x36, 0x30, 0x32}, {0x32, 0x35, 0x32, 0x35},
    {0x32, 0x34, 0x34, 0x39}, {0x32, 0x34, 0x38, 0x09},
    {0x32, 0x35, 0x35, 0x36}, {0x32, 0x36, 0x30, 0x32},
    {0x32, 0x36, 0x37, 0x09}, {0x32, 0x37, 0x33, 0x39},
    {0x32, 0x37, 0x36, 0x39}, {0x32, 0x37, 0x34, 0x37},
    {0x32, 0x36, 0x37, 0x09}, {0x32, 0x35, 0x39, 0x34},
    {0x32, 0x35, 0x31, 0x38}, {0x32, 0x34, 0x34, 0x31},
    {0x32, 0x33, 0x38, 0x38}, {0x32, 0x34, 0x33, 0x34},
    {0x32, 0x35, 0x31, 0x09}, {0x32, 0x35, 0x38, 0x36},
    {0x32, 0x36, 0x35, 0x35}, {0x32, 0x37, 0x33, 0x31},
    {0x32, 0x38, 0x30, 0x38}, {0x32, 0x38, 0x33, 0x31},
    {0x32, 0x37, 0x35, 0x34}, {0x32, 0x36, 0x37, 0x38},
    {0x32, 0x36, 0x30, 0x32}, {0x32, 0x35, 0x32, 0x35},
    {0x32, 0x34, 0x34, 0x39}, {0x32, 0x33, 0x37, 0x33},
    {0x32, 0x33, 0x30, 0x34}, {0x32, 0x33, 0x32, 0x37},
    {0x32, 0x34, 0x30, 0x33}, {0x32, 0x34, 0x38, 0x09},
    {0x32, 0x35, 0x35, 0x36}, {0x32, 0x36, 0x32, 0x35},
    {0x32, 0x37, 0x30, 0x31}, {0x32, 0x37, 0x37, 0x37},
    {0x32, 0x38, 0x30, 0x38}, {0x32, 0x37, 0x36, 0x32},
    {0x32, 0x36, 0x38, 0x36}, {0x32, 0x36, 0x30, 0x39},
    {0x32, 0x35, 0x33, 0x33}, {0x32, 0x34, 0x35, 0x37},
    {0x32, 0x33, 0x38, 0x09}, {0x32, 0x33, 0x35, 0x09},
    {0x32, 0x32, 0x37, 0x34}, {0x32, 0x31, 0x39, 0x37},
    {0x32, 0x31, 0x32, 0x31}, {0x32, 0x30, 0x34, 0x35},
    {0x32, 0x30, 0x30, 0x37}, {0x32, 0x30, 0x35, 0x32},
    {0x32, 0x31, 0x32, 0x39}, {0x32, 0x32, 0x30, 0x35},
    {0x32, 0x32, 0x37, 0x34}, {0x32, 0x33, 0x35, 0x09},
    {0x32, 0x34, 0x32, 0x36}, {0x32, 0x34, 0x36, 0x34},
    {0x32, 0x34, 0x32, 0x36}, {0x32, 0x33, 0x31, 0x32},
    {0x32, 0x31, 0x35, 0x39}, {0x32, 0x30, 0x37, 0x35},
    {0x32, 0x30, 0x39, 0x38}, {0x32, 0x31, 0x37, 0x34},
    {0x32, 0x32, 0x34, 0x33}, {0x32, 0x33, 0x31, 0x39},
    {0x32, 0x33, 0x39, 0x36}, {0x32, 0x34, 0x35, 0x37},
    {0x32, 0x34, 0x34, 0x39}, {0x32, 0x33, 0x37, 0x33},
    {0x32, 0x32, 0x39, 0x36}, {0x32, 0x32, 0x32, 0x09},
    {0x32, 0x31, 0x34, 0x34}, {0x32, 0x30, 0x38, 0x33},
    {0x32, 0x31, 0x32, 0x39}, {0x32, 0x32, 0x30, 0x35},
    {0x32, 0x32, 0x38, 0x31}, {0x32, 0x33, 0x35, 0x37},
    {0x32, 0x34, 0x33, 0x34}, {0x32, 0x34, 0x34, 0x31},
    {0x32, 0x33, 0x36, 0x35}, {0x32, 0x32, 0x38, 0x39},
    {0x32, 0x31, 0x37, 0x34}, {0x32, 0x30, 0x32, 0x39},
    {0x31, 0x39, 0x35, 0x33}, {0x31, 0x38, 0x37, 0x37},
    {0x31, 0x38, 0x33, 0x31}, {0x31, 0x38, 0x31, 0x36},
    {0x31, 0x37, 0x34, 0x09}, {0x31, 0x36, 0x36, 0x33},
    {0x31, 0x35, 0x38, 0x37}, {0x31, 0x35, 0x31, 0x31},
    {0x31, 0x34, 0x38, 0x09}, {0x31, 0x35, 0x32, 0x36},
    {0x31, 0x36, 0x30, 0x32}, {0x31, 0x36, 0x37, 0x38},
    {0x31, 0x37, 0x35, 0x35}, {0x31, 0x38, 0x33, 0x31},
    {0x31, 0x39, 0x30, 0x37}};

/** 
 * Lines at which new unique portions of the data from AEP_10.ascii from
 * ImpBench occur. Used to generate the original ImpBench data in
 * data_gen_get8()
 */
static const uint16_t data_lines[] = {
    169,  170,  171,  172,  173,  174,  175,   176,  177,  178,  179,  180,
    181,  182,  184,  185,  186,  187,  188,   189,  192,  193,  194,  195,
    196,  197,  198,  202,  203,  204,  205,   206,  207,  208,  212,  213,
    214,  215,  216,  217,  218,  219,  220,   221,  222,  223,  224,  225,
    226,  599,  600,  601,  602,  603,  618,   619,  620,  621,  622,  623,
    624,  630,  631,  632,  633,  638,  639,   640,  641,  642,  643,  649,
    650,  651,  652,  653,  654,  661,  662,   663,  664,  665,  666,  667,
    668,  669,  670,  671,  672,  673,  1024,  1025, 1026, 1027, 1028, 1029,
    1078, 1079, 1080, 1081, 1082, 1083, 0xFFFF};

/**
 * Generate a pseudo-random number
 * Based on the implementation by Synopsys, depends on the static variable next
 * To get consistent results, call data_gen_reset() at the start of each
 * benchmark
 *
 * next is the seed for the next RV, used by rand()
 * Gets reset by data_gen_reset()
 *
 * @returns a 32-bit pseudo-random variable
 */
#ifdef __clang__
static volatile uint32_t next = 1;
uint32_t rand(void) {
    next = next * 1103515245 + 12345;
    return next;
}
#else
#ifdef __chess__
static uint32_t next = 1;
uint32_t rand(void) {
    next = chess_copy(next) * 1103515245 + 12345;
    return next;
}
#else
#ifdef __tce__
// use the argument "-k next" in compilation
uint32_t next = 1;
uint32_t rand(void) {
    next = chess_copy(next) * 1103515245 + 12345;
    return next;
}
#else
// any other system will use volatile
static volatile uint32_t next = 1;
uint32_t rand(void) {
    next = next * 1103515245 + 12345;
    return next;
}
#endif // __tce__
#endif // __chess__
#endif // __clang__

/**
 * Generate a pseudo-random 32-bit number
 * To get consistent results, call data_gen_reset() at the start of each
 * benchmark
 *
 * @returns a pseudo-random 32-bit number
 */
uint32_t data_gen32(void) {
    return rand();
}

/**
 * Reset the data generation
 * Should be called at the start of every benchmark using data generation
 * functions from this file to make sure the data used is consistent.
 */
void data_gen_reset(void) {
    // reset the random number generator
    next = 1;
    // reset the impbench-like number generator
    line_idx = 0;
    column_idx = 0;
    data_mid_idx = 0;
    data_lines_idx = 0;
    // set the endianness
    little_endian = endian_is_little();
}

/**
 * Simulates the reading of one byte from AEP_10.ascii
 * To get consistent results, call data_gen_reset() at the start of each
 * benchmark
 *
 * @returns one byte as if read from AEP_10.ascii
 */
static uint8_t data_gen_get8(void) {
    // all lines start with "90."
    uint8_t common_start[] = {0x39, 0x30, 0x2E};
    // all lines end with "\t\r\n"
    uint8_t common_end[] = {0x09, 0x0D, 0x0A};
    uint8_t res;
    // if we reached the last line of occurrence of the last line, go to the
    // next
    if (line_idx == data_lines[data_lines_idx]) {
        data_mid_idx++;
        data_lines_idx++;
    }
    // if this line only has 3 decimal characters, go to the common_end (by
    // increasing the column_idx by one)
    if (column_idx == 6 && data_mid[data_mid_idx][3] == 0x09)
        column_idx++;
    // now get the data
    if (column_idx <= 2)
        // common 3 start bytes (i.e. "90.")
        res = common_start[column_idx];
    else if (column_idx >= 7)
        // common 3 end bytes (i.e. \t\r\n)
        res = common_end[column_idx - 7];
    else
        // unique 3-4 mid bytes
        res = data_mid[data_mid_idx][column_idx - 3];
    if (data_mid_idx < sizeof(data_lines) / sizeof(uint16_t))
        column_idx++;
    if (column_idx > 9) {
        column_idx = 0;
        line_idx++;
    }
    return res;
}

/**
 * Fill a buffer with data for benchmarks
 *
 * Fills a buffer *buf of size len with data from AEP_10.ascii or pseudo-random
 * data if DATA_GEN_RANDOM is not 0.
 * When DATA_GEN_RANDOM is 0, the datasize argument is used to re-order the
 * bytes for big-endian architectures.
 * To get consistent results, call data_gen_reset() at the start of each
 * benchmark
 *
 * @p buf a pointer to the buffer to be filled
 * @p len the size of the buffer in bytes
 */
void data_gen_buf(uint32_t* buf, size_t len) {
    size_t i;
#if DATA_GEN_RANDOM
    uint32_t r;
    uint32_t mask = 0;
    // fill up buf 8 bytes at a time
    for (i = 0; i < len / 8; i++) {
        r = rand();
        // only take last 4 bits per byte, so there are only 16 possible
        // outcomes. this reduces memory footprint for compression benchmarks
        buf[i] = r & 0x0F0F0F0F;
        i++;
        buf[i] = (r >> 4) & 0x0F0F0F0F;
    }
    // fill-up left-over bytes
    r = rand();
    if(len % 8 >= 4) {
        buf[i] = r & 0x0F0F0F0F;
        i++;
        r >>= 4;
    }
    for (i = 0; i < len % 4; i++) {
        mask |= 0x0F << (i*8);
    }
    buf[i] = r & mask;
#else
    // call data_gen_get8() in a loop until all bytes in buf are filled
    uint32_t j;
    for (i = 0; i < len / 4; i++) {
        buf[i] = (uint32_t)data_gen_get8();
        buf[i] |= (uint32_t)data_gen_get8() << 8;
        buf[i] |= (uint32_t)data_gen_get8() << 16;
        buf[i] |= (uint32_t)data_gen_get8() << 24;
    }
    if(len %4)
        buf[i] = 0;
    for (j = 0; j < len % 4; j++) {
        buf[i] |= (uint32_t)data_gen_get8() << (j*8);
    }
#endif
    return;
}

