/**
 * @file 3lnn.c
 * @brief Neural network functionality for a 3-layer (INPUT, HIDDEN, OUTPUT)
 * feed-forward, back-prop NN
 * @author Matt Lind
 * @date August 2015
 */

#include <stdint.h>
#include <string.h>

#include "print.h"

#include "cnn.h"
#include "q.h"

#include "mnist.h"
#include "network49.h"

#define ACTTYPE_SIGMOID
//#define ACTTYPE_TANH

/**
 * @details Returns the next image in the given MNIST image file
 */
static MNIST_Image getImage(size_t i) {
    return (MNIST_Image)img_test[i];
}

/**
 * @details Returns the next label in the given MNIST label file
 */
static MNIST_Label getLabel(size_t i) {
    return (MNIST_Label)(lbl_test[i / 2] & (i % 2 ? 0xF0 : 0x0F)) >>
               (i % 2 ? 4 : 0);
}

/**
 * @details Retrieves a node via ID from a layer
 */
static Node* getNode(Layer* l, int nodeId) {
    return &l->nodes[nodeId];
}

/**
 * @brief Returns one of the layers of the network
 * @param nn A pointer to the NN
 * @param ltype Type of layer to be returned (INPUT, HIDDEN, OUTPUT)
 */
static Layer* getLayer(Network* nn, LayerType ltype) {
    return &nn->layers[ltype - 1];
}

/**
 * @brief Returns the result of applying the given outputValue to the derivate
 * of the activation function
 * @param nn A pointer to the NN
 * @param ltype Type of layer (INPUT, HIDDEN, OUTPUT)
 * @param outVal Output value that is to be back propagated
 */
static q_t getActFctDerivative(Network* nn, LayerType ltype, q_t outVal) {
    q_t dVal = 0;
    (void)ltype;
    (void)nn;
#ifdef ACTTYPE_TANH
    q_t tmp = q_exp(q_mul(2 * q_one, outVal));
    tmp = q_div(q_sub(tmp, q_one), q_add(tmp, q_one));
    tmp = q_mul(tmp, tmp);
    dVal = q_sub(q_one, tmp);
#else
    dVal = q_mul(outVal, q_sub(q_one, outVal));
#endif

    return dVal;
}

/**
 * @brief Updates a node's weights based on given error
 * @param nn A pointer to the NN
 * @param ltype Type of layer (INPUT, HIDDEN, OUTPUT)
 * @param id Sequential id of the node that is to be calculated
 * @param error The error (difference between desired output and actual output
 */
static void updateNodeWeights(Network* nn, LayerType ltype, int id, q_t error) {
    Layer* updateLayer = getLayer(nn, ltype);
    Node* updateNode = getNode(updateLayer, id);

    Layer* prevLayer;
    if (ltype == HIDDEN) {
        int it0 = 0, it1 = 0;
        for (int i = 0; i < updateNode->wcount; i++) {
            // get the pixel value, i.e. the previous layer (input layer)'s
            // output.
            int out = nn->input_img[it0] & (1 << it1);
            it1++;
            if (it1 > mnist_data_width) {
                it1 = 0;
                it0++;
            }
            // we reduced the value to 1 bit, so the entire addition to weights
            // is discarded if the value is 0. also we don't have to multiply
            // with the value anymore as it would be 1.
            if (out) {
                updateNode->weights[i] = q_add(updateNode->weights[i],
                                               q_mul(nn->learningRate, error));
            }
        }

        // update bias weight
        updateNode->bias =
            q_add(updateNode->bias, q_mul(nn->learningRate, error));

    } else {
        // output layer
        prevLayer = getLayer(nn, HIDDEN);

        for (int i = 0; i < updateNode->wcount; i++) {
            Node* prevLayerNode = getNode(prevLayer, i);
            updateNode->weights[i] = q_add(
                updateNode->weights[i],
                q_mul(nn->learningRate, q_mul(prevLayerNode->output, error)));
        }

        // update bias weight
        updateNode->bias =
            q_add(updateNode->bias, q_mul(nn->learningRate, error));
        return;
    }
}

/**
 * @brief Back propagates network error to hidden layer
 * @param nn A pointer to the NN
 * @param targetClassification Correct classification (=label) of the input
 * stream
 */
static void backPropagateHiddenLayer(Network* nn, int targetClassification) {
    Layer* ol = getLayer(nn, OUTPUT);
    Layer* hl = getLayer(nn, HIDDEN);

    for (int h = 0; h < hl->ncount; h++) {
        Node* hn = getNode(hl, h);

        q_t outputcellerrorsum = 0;

        for (int o = 0; o < ol->ncount; o++) {
            Node* on = getNode(ol, o);

            q_t targetOutput = (o == targetClassification) ? q_one : 0;

            q_t errorDelta = q_sub(targetOutput, on->output);
            q_t errorSignal =
                q_mul(errorDelta, getActFctDerivative(nn, OUTPUT, on->output));

            outputcellerrorsum =
                q_add(outputcellerrorsum, q_mul(errorSignal, on->weights[h]));
        }

        q_t hiddenErrorSignal = q_mul(
            outputcellerrorsum, getActFctDerivative(nn, HIDDEN, hn->output));
        updateNodeWeights(nn, HIDDEN, h, hiddenErrorSignal);
    }
}

/**
 * @brief Back propagates network error in output layer
 * @param nn A pointer to the NN
 * @param targetClassification Correct classification (=label) of the input
 * stream
 */
static void backPropagateOutputLayer(Network* nn, int targetClassification) {
    Layer* ol = getLayer(nn, OUTPUT);

    for (int o = 0; o < ol->ncount; o++) {
        Node* on = getNode(ol, o);

        q_t targetOutput = (o == targetClassification) ? q_one : 0;

        q_t errorDelta = q_sub(targetOutput, on->output);
        q_t errorSignal =
            q_mul(errorDelta, getActFctDerivative(nn, OUTPUT, on->output));
        updateNodeWeights(nn, OUTPUT, o, errorSignal);
    }
}

/**
 * @brief Back propagates network error from output layer to hidden layer
 * @param nn A pointer to the NN
 * @param targetClassification Correct classification (=label) of the input
 * stream
 */
void backPropagateNetwork(Network* nn, int targetClassification) {
    backPropagateOutputLayer(nn, targetClassification);

    backPropagateHiddenLayer(nn, targetClassification);
}

/**
 * @brief Performs an activiation function (as defined in the NN's defaults) to
 * a specified node
 * @param nn A pointer to the NN
 * @param ltype Type of layer (INPUT, HIDDEN, OUTPUT)
 * @param id Sequential id of the node that is to be calculated
 */
static void activateNode(Network* nn, LayerType ltype, int id) {
    Layer* l = getLayer(nn, ltype);
    Node* n = getNode(l, id);
    /*
    ActFctType actFct;
    if (ltype == HIDDEN)
        actFct = nn->hidLayerActType;
    else
        actFct = nn->outLayerActType;
    */

#ifdef ACTTYPE_TANH
    q_t tmp = q_exp(q_mul(2 * q_one, n->output));
    tmp = q_div(q_sub(tmp, q_one), q_add(tmp, q_one));
    n->output = tmp;
#else
    n->output = q_sigmoid(n->output);
#endif
}

/**
 * @brief Calculates the output value of a specified node by multiplying all its
 * weights with the previous layer's outputs
 * @param nn A pointer to the NN
 * @param ltype Type of layer (INPUT, HIDDEN, OUTPUT)
 * @param id Sequential id of the node that is to be calculated
 */
static void calcNodeOutput(Network* nn, LayerType ltype, int id) {
    Layer* calcLayer = getLayer(nn, ltype);
    Node* calcNode = getNode(calcLayer, id);

    Layer* prevLayer;

    if (ltype == HIDDEN) {
        calcNode->output = calcNode->bias;

        uint32_t it0 = 0, it1 = 0;
        for (int i = 0; i < nn->input_size; i++) {
            // get the pixel value, i.e. the previous layer (input layer)'s
            // output.
            uint32_t out = nn->input_img[it0] & ((uint32_t)1 << it1);
            it1++;
            if (it1 > mnist_data_width) {
                it1 = 0;
                it0++;
            }
            // we reduced the value to 1 bit, so the entire addition to weights
            // is discarded if the value is 0. Also we don't have to multiply
            // with the value anymore as 1*weight = weight.
            if (out) {
                calcNode->output =
                    q_add(calcNode->output, calcNode->weights[i]);
            }
        }
    } else {
        prevLayer = getLayer(nn, HIDDEN);

        // Start by adding the bias
        calcNode->output = calcNode->bias;

        for (int i = 0; i < prevLayer->ncount; i++) {
            Node* prevLayerNode = getNode(prevLayer, i);
            calcNode->output =
                q_add(calcNode->output,
                      q_mul(prevLayerNode->output, calcNode->weights[i]));
        }
        return;
    }
}

/**
 * @brief Calculates the output values of a given NN layer
 * @param nn A pointer to the NN
 * @param ltype Type of layer (INPUT, HIDDEN, OUTPUT)
 */
static void calcLayer(Network* nn, LayerType ltype) {
    Layer* l;
    l = getLayer(nn, ltype);

    for (int i = 0; i < l->ncount; i++) {
        calcNodeOutput(nn, ltype, i);
        activateNode(nn, ltype, i);
    }
}

/**
 * @brief Feeds input layer values forward to hidden to output layer
 * (calculation and activation fct)
 * @param nn A pointer to the NN
 */
void feedForwardNetwork(Network* nn) {
    calcLayer(nn, HIDDEN);
    calcLayer(nn, OUTPUT);
}

/**
 * @brief Feeds some Vector data into the INPUT layer of the NN
 * @param nn A pointer to the NN
 * @param v A pointer to a vector
 */
void feedInput(Network* nn, MNIST_Image img) {
    nn->input_img = img;
}

/**
 * @brief Returns the network's classification using the ID of teh node with the
 * hightest output
 * @param nn A pointer to the NN
 */
int getNetworkClassification(Network* nn) {
    Layer* l = getLayer(nn, OUTPUT);

    q_t maxOut = SAT_MIN;
    int maxInd = 0;

    for (int i = 0; i < l->ncount; i++) {
        Node* on = getNode(l, i);

        if (on->output > maxOut) {
            maxOut = on->output;
            maxInd = i;
        }
    }

    return maxInd;
}

int32_t cnn(void) {
    int errCount = 0;
    Network* nn = &network;

    // Loop through all images in the file
    for (int imgCount = 0; imgCount < num_test_imgs; imgCount++) {
        // Reading next image and its corresponding label
        MNIST_Image img = getImage(imgCount);
        MNIST_Label lbl = getLabel(imgCount);

        // Convert the MNIST image to a standardized vector format and feed into
        // the network
        feedInput(nn, img);

        // Feed forward all layers (from input to hidden to output) calculating
        // all nodes' output
        feedForwardNetwork(nn);
        
        // Classify image by choosing output cell with highest output
        int classification = getNetworkClassification(nn);
        if (classification != lbl)
            errCount++;
    }
    return errCount > 19;
}

