/*  + Name:
 *  ImpBench/Motion
 *
 *  + Details:
 *  Motion-detection algorithm for movement of animals, based on the description
 * given in the paper: R. Puers and P. Wouters, "Adaptable interface Circuits
 * for Flexible Monitoring of Temperature and Movement", AICSP, Vol. 14, pp.
 * 193--206, 1997. This is a kernel program. It does (and can) not simulate
 * real-time time aspects of the actual (hardware) system, such as accelerometer
 * interrupts. In this algorithm, the degree of activity is actually monitored
 * rather than the exact value of the amplitude of the activiy signal. That is,
 * the percentage of samples above a set threshold value in a given monitoring
 * window. In effect, this motion-detection algorithm is a data-reduction
 * algorithm. Some algorithm parameters (such as the threshold, the sampling
 * rate etc.) have been empirically decided upon, since the actual paper does
 * not give specific information on them.
 *
 *  The program assumes ASCII input files containing a single column of
 * floating-point numbers, e.g. -0.00213623 -0.000305176 0.00146484 0.00299072
 *  0.00445557
 *  ...
 *  Particular delimitation at the end of each line is not important. It can be:
 *  <CR>, <CR>+<LF>, <TAB>+<CR> or <TAB>+<CR>+<LF> etc..
 *  For the output format see comments in the source code.
 *
 *  To-Do list:
 *  - There is a known issue with binary data that remains to be fixed.
 *
 *  + Syntax:
 *  <exec> <motion-sensor data in filename> <transmission out filename>
 *
 *  + Copyright:
 *  The algorithm concept is the property of the paper authors, as stated above.
 *  This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2008. All rights reserved (C).
 */

#include "motion.h"
#include <stdint.h>
#include "data_gen.h"

// Define parameters of the modeled system (and avoid multiplications during
// program execution)
#define THRESHOLD (0x80000000)
#define SAMPLING_RATE 10     // Hz
#define MONITORING_SAMPLE_PERIODS 4096
#define MONITORING_PERIOD 2  // sec (range: [1..512] sec)
#define MONITORING_SAMPLES \
    (SAMPLING_RATE * MONITORING_PERIOD)  // #samples to be monitored during the
                                         // monitoring cycle
#define byte_round_cast(i) ((uint8_t)((i) + 0.5))

// real-app test07: > ./benchmarks/motion/motion ./files_input/AEP_10.asci
// ./files_output/test07.out
int32_t motion(void) {
    uint32_t sample;
    uint32_t readout;
    uint32_t active_counter = 0;
    uint32_t motion_counter = 0;
    uint32_t res = 0;
    // make sure the threshold is a uint32_t
    uint32_t threshold = THRESHOLD;
    // reset pseudo-RNG seed
    data_gen_reset();
    for (sample =0; sample < MONITORING_SAMPLE_PERIODS; sample++) {
        // enter new monitoring mode
        while (active_counter < MONITORING_SAMPLES) {
            // read new sensory data
            readout = rand();
            // check if new readout is above the threshold value
            if (readout >= threshold)
                motion_counter++;
            active_counter++;
        }
        // add "active measurements" to result
        res += motion_counter;
        // reset counters for next monitoring session
        active_counter = 0;
        motion_counter = 0;
        // idle mode is removed
    }
    return res != 40789;
}
