benches=("crc" "csum" "misty1" "motion" "rc6" "fin" "coremark")
benchmarks=("CRC32" "CSUM" "MISTY1" "Motion" "RC6" "Finnish" "CoreMark")
functions=("update_crc" "checksum" "MISTY1" "Motion" "RC6" "Finnish" "CoreMark")
cores=("pelotta" "tdsp" "tvliw" "tzscale")
core_names=("PeLoTTA" "Tdsp" "Tvliw" "Tzscale")

for b in {0..5}; do
    bench=${benches[$b]}
    benchmark=${benchmarks[$b]}
    out=tables/${bench}_profiling.tex
    echo "
\begin{table}[H]
\centering
\caption{Analysis of the for-loop in \t{()}, in the $benchmark benchmark.
    The data is averaged for one loop iteration.}
\label{tab:${bench}_profiling}
\begin{tabular}{l|llll}
Core    & Cycle count & Operations & Avg. ops/cycle & Unroll factor \\\\ \hline" > $out
    for c in {0..3}; do
        core=${cores[$c]}
        core_name=${core_names[$c]}
        stats=$(cat profiling/${core}_${bench}_stat)
        if [[ $c < 3 ]]; then
            end="\\\\"
        else
            end=""
        fi
        echo \
"$core_name & $stats $end" >> $out
    done
    echo \
"\end{tabular}
\end{table}
" >> $out
done
