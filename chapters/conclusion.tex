\chapter{Conclusions and recommendations}\label{sec:conclusion}
    This chapter concludes the thesis by providing an overview of the work in
    this thesis, reflecting on the problem statement in
    Chapter~\ref{sec:intro}, stating the contributions, and suggesting
    recommendations.

    \section{Thesis overview}
        Chapter~\ref{sec:intro} presented the motivation and context of this
        thesis, and introduced the problem statement.
        Chapter~\ref{sec:background} provided the necessary background
        information to the problem statement and its sub-questions; which are
        answered as follows:\\
        \textit{Which benchmarks need to be run to represent the workloads of
        \glspl{IMD}}?\\
        Workloads of \glspl{IMD} are categorised in six categories; control,
        encryption, data compression, error detection/correction, and
        \glspl{ANN}.
        The benchmark suites selected that represent these workloads are
        ImpBench and CoreMark.  
        A design and implementation of an additional benchmark for \glspl{ANN}
        is also presented in this thesis.

        \textit{Which processor architectures are currently prevalent in
        \glspl{IMD}?}\\
        Most processor-based \glspl{IMD} use microcontrollers containing
        processors with \gls{RISC} architectures.
        Besides \gls{RISC}, there are also few occurrences of \gls{CISC} and
        \gls{DSP} architectures.

        \textit{Which low-power implementations of computer architectures are
        available for \glspl{ASIP}?}\\
        ASIP Designer provides many standard processor models for \glspl{ASIP}
        based on architectures like \gls{VLIW}, \gls{RISC}, \gls{DSP}, and has
        the option to optimise the processor implementation for low-power.
        OpenASIP provides \gls{TTA} \gls{ASIP} implementations on top of these
        architectures.

        Chapter~\ref{sec:altsol} reviewed alternatives to \glspl{ASIP} and
        selects implementations of the desired architectures.
        To represent the current state of processors in \glspl{IMD} the Tzscale
        with its RISC-V RV32I instruction set and the Tdsp with its \gls{DSP}
        \gls{ISA} are selected.
        As a viable alternative the PeLoTTA is selected, which is a low-power
        \gls{TTA} core.
        The Tvliw is added to the comparison to see the performance of another
        \gls{VLIW} processor and to bridge the architectural gap in the
        comparison.
        The chapter concluded with an overview of benchmark suites and the
        selection thereof.

        In Chapter~\ref{sec:implementation} the selected processor
        implementations are implemented to run on an \gls{FPGA}.
        The benchmarks in ImpBench are ported to be able to run without OS on a
        processor with a very small memory footprint.
        A benchmark for neural networks is also implemented, using a \gls{CNN}
        on the MNIST dataset.
        Chapter~\ref{sec:results} presents the experimental setup and the
        simulation and power measurement results, and are discussed at the end
        of the chapter.
        This finally leads to the answering of the core question in the problem
        statement:
        \textit{How do different \gls{ASIP} computer architectures compare for
        medical implantable devices?}
        For systems running simple control applications or compression
        workloads, the RISC-V ISA of the Tzscale processor performs well.
        It combines low area requirements and good performance, and delivers
        this on a low energy budget.
        The Tzscale also achieves the highest CoreMark score, with equivalent
        performance to the ARM Cortex-M0+.
        The PeLoTTA surpasses the Tzscale in terms of area numbers with roughly
        two-thirds of the \glspl{LE} used, and in terms of power efficiency in
        the data validation, encryption, and neural network benchmarks.
        For CoreMark, the PeLoTTA achieves comparable power efficiency compared
        to the Tzscale.
        The PeLoTTA does have some downsides, as it is does not support
        interrupts and requires more program memory.
        
        The Tvliw and Tdsp would not be recommended for any implementations in
        \glspl{IMD}, as they use more than twice the \glspl{LE} than the
        PeLoTTA, but deliver only mediocre performance for the extra resources
        in most benchmarks.
        It must be noted however, that the Tvliw lacks a good compiler
        front-end, and is not fully able to exploit its \gls{ILP} because of
        this.

        This conclusion is largely based on measurements of the implementations
        on an \gls{FPGA}.
        This approach does have its shortcomings in that it may not provide an
        accurate representation for implementations on \glspl{ASIC}, but does
        provide an indication of the relative area and performance of the
        implementations.
        
    \section{Contributions}
        In this thesis a novel collection of benchmarks is used to assess
        implementations of a set of different \gls{ASIP} architectures.
        This benchmark collection consists of the benchmarks from ImpBench,
        CoreMark, and a custom benchmark for \glspl{CNN}.
        Benchmarks from ImpBench were ported to be completely
        architecture-agnostic.
        The new \gls{CNN} benchmark represents the well-known workload of using
        a \gls{CNN} for pattern recognition.
        The benchmark uses fixed-point arithmetic and is also developed to be
        architecture-agnostic and have a small memory footprint.

        This thesis also presents the first use of this collection of
        benchmarks on implementations of a set of different \glspl{ASIP}
        architectures.
        Next to simulations, power measurements are also performed by
        running the benchmarks on the \gls{ASIP} implementations on
        \glspl{FPGA}
        This provides insight on the performance of \gls{ASIP} architectures
        for \glspl{IMD}, which is the goal of this thesis.

    \section{Recommendations}
        The power measurements of the benchmarks in ImpBench running on the
        implementations on the \gls{FPGA} also measure the clock cycles
        dedicated to data generation.
        A better approach would be to generate the data completely and let the
        benchmark iterate several times over the data, to lower the overhead of
        data generation.

        During the porting of ImpBench, two benchmarks were left unimplemented:
        MLZO, which claimed too much memory, and DMU, which contained too many
        floating-point operations.
        Although other benchmarks are included which cover their
        targeted workload, it would be good to have multiple benchmarks cover
        the same workload.
        For the same reason, it would be good to run a second \gls{ANN}
        benchmark with a different architecture than the \gls{CNN} benchmark.
        The \gls{CNN} benchmark could also be improved to better match the true
        workload of \glspl{CNN} in \glspl{IMD} by replicating real applications
        and using real samples.

        For the architectural decision for \glspl{ASIP} for \glspl{IMD}, it is
        recommended to extend the PeLoTTA and Tzscale with application-specific
        instructions.
        The performance of the processors can then be re-evaluated to see the
        effect of the extended instruction set and the relative performance
        gain of the processors.
        After this stage, implementation and simulations of \glspl{ASIC} would
        provide more accurate power and area numbers, and play a decisive role
        in the choice of \gls{ASIP} architecture for \glspl{IMD}.

