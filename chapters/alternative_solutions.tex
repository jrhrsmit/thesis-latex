\chapter{Alternative solutions}\label{sec:altsol}
    There are many viable solutions for implementing the functionality of an
    \gls{IMD}.
    This chapter aims to provide an overview of these solutions and compare
    them.
    The comparison will form the basis for selecting the most promising
    solutions, which will be implemented in the next chapter.
    
    To acquire performance metrics of the implemented cores, benchmarks will
    have to be run on the selected cores.
    Suitable benchmark suites will be presented, of which some will be selected
    to run on the cores.
    Finally, techniques will be discussed to obtain area and power numbers.

    \section{IMD platforms}
        An classification of solutions that will be reviewed can be found in
        Figure~\ref{fig:platforms}.
        \input{graphs/platforms.tex}

        \subsection{Requirements}
            The background in Chapter~\ref{sec:background} produces the
            following requirements for \glspl{IMD}:
            \begin{itemize}
                \item High reliability
                \item Real-time performance (no deadline misses)
                \item Low peak and average power consumption
                \item Low area use
                \item Low design effort
            \end{itemize}
            For \glspl{IMD} the most important metric is reliability.
            The requirement of reliability in combination with real-time
            performance implies that he \gls{IMD} can not miss any deadlines.
            Context switching degrades the reliability of processors, as it is
            harder to provide guarantees on meeting deadlines and thus is
            something to avoid in \glspl{IMD}.
            
            The increasing efficiency of silicon technology benefits most of
            the solutions in terms of area and power consumption, thus there
            are multiple viable options that will be discussed.

        \subsection{Hardwired datapaths}
            The entire system could be described in a \gls{FSM} or
            combinational logic with a \gls{HDL} such as Verilog or \gls{VHDL}.
            The hardware description could be synthesized directly on an
            \gls{ASIC}, \gls{CPLD}, or on an \gls{FPGA} .
            This approach scores very high in terms of reliability, real-time
            performance and power consumption, but the design process can be
            very tedious compared to traditional programming of general-purpose
            processors.
            Due to the system being designed for one specific purpose, the
            solution is very inflexible.
            
            When combinational logic or an \gls{FSM} is implemented in an
            \glspl{ASIC}, it usually outperforms any other solution.
            However, updates or changes to the design requires new expensive
            research and development, and old implementations can not be
            updated as the \gls{ASIC} is not programmable.

            Although \glspl{CPLD} and \glspl{FPGA} can be reprogrammed, they
            still lack flexibility in terms of design effort.
            A small change to a complex algorithm in software is likely to cost
            less time than it would in a hardwired datapath.

        \subsection{General-purpose processors}
            \Glspl{GPP} are useful for their quick time-to-market compared to
            custom solutions.
            Especially well-known architectures like x86 or MIPS are supported
            by multiple compilers and debuggers that support high-level
            object-oriented languages, and reduce the design effort for the
            programmer.
            The inherent downside of the \gls{GPP} is the higher power
            consumption compared to application-specific solutions.

            \Gls{GPP} are available in \glspl{MCU}, which is a popular choice
            for \glspl{IMD}.
            For commercial \glspl{MCU}, development boards and example programs
            are readily available and they are usually well documented.
            On top of that they are often in stock at large distributors or the
            manufacturer themselves, which enables large production volumes at
            short notice.
        
        \subsection{ASIP}
            The middle ground between hardwired datapaths and \glspl{GPP} is
            owned by programmable application-specific processors; the
            \glspl{ASIP}.
            \Glspl{ASIP} have the benefits and drawbacks of both worlds;
            development is lengthy and costly, but only has to be done once for
            a range of solutions, and they can be reprogrammed.
            \Glspl{ASIP} are defined by their unique instruction set, which 
            usually contains instructions developed for the application.
            The tailored instruction set makes \glspl{ASIP} more power
            efficient than \glspl{GPP}, while maintaining programmability.

        \subsection{Platform comparison}
            The assessed platforms are qualitatively analysed for each
            requirement in Table~\ref{tab:solutions_comparison}.
            The design effort for implementations with hardwired datapaths is
            too high, which is the primary reason for the prevalence of
            \glspl{GPP} in \gls{IMD}.
            The \gls{ASIP} do perform better in both power consumption and
            real-time performance than \glspl{GPP}, but sacrifice design
            effort.
            Especially the increased power efficiency makes the \gls{ASIP} an
            interesting choice for \glspl{IMD}.
            \input{tables/solution_comparison.tex}
    
        \section{ASIP architectures and implementations}
            The following architectures will be compared in this thesis:
            \begin{itemize}
                \item \Gls{RISC}\\
                    As seen in Section~\ref{sec:proc_imds}, \Gls{GPP} in
                    \glspl{ASIP} are mostly \gls{RISC}.
                    \Gls{RISC} is not only popular amongst processor
                    architectures in \glspl{MCU} in \glspl{IMD}, but also as a
                    base for \glspl{ASIP}.
                    For this reason, a \gls{RISC} architecture will be
                    selected.
                \item \Gls{DSP}\\
                    Some \glspl{IMD} employ \glspl{DSP} for audio processing in
                    cochlear implants or signal processing of \glspl{ECG}, thus
                    it would be interesting to see how a \gls{DSP} would fare
                    in this comparison.
                \item \Gls{TTA}\\
                    \Glspl{TTA} show promise as low-power \glspl{ASIP} due to
                    their inherent \gls{ILP} and reduced complexity compared to
                    a \gls{VLIW}.
                \item \Gls{VLIW}\\
                    To bridge the architectural gap between \gls{RISC} and
                    \gls{TTA}, a \glspl{VLIW} architecture will also be
                    selected.
            \end{itemize}
            Although there are also some cases of \gls{CISC} architectures to
            be found in \glspl{IMD} \cite{mcu2}, a \gls{CISC} typically
            uses a lot of area and is already extended with complex
            instructions, so it does not form a good base architecture for an
            \gls{ASIP}.

            Popular design tools for \glspl{ASIP} are Synopsys ASIP Designer
            and the open-source toolset \gls{TCE} for \gls{TTA}.
            From these toolsets some implementations will be presented.

            \subsection{ASIP Designer}
                ASIP Designer provides some example projects that implement
                some popular architectures that could be used to compare the
                TTA core against.
                Noteworthy examples are:
                \begin{itemize}
                    \item Tmcu, 32-bit microcontroller with a Harvard
                        architecture and 3-stage pipeline (IF/DE/EX). 
                        Supports variable length and parallel instructions.
                        The Tmcu has 16 32-bit registers and byte addressable
                        data memory.
                    \item Tzscale, implementation of the RISC-V ISA with a
                        3-stage pipeline (IF/DE/EX).
                        Also supports variable length instructions, and has 16
                        or 32 32-bit registers and byte addressable data
                        memory.
                    \item Tvliw, implementation of a 4-slot VLIW (2 \glspl{ALU}, 2
                        \glspl{LSU}).
                        Optional variable length instructions, predicated
                        execution, and two stage program memory fetch.
                    \item Tdsp, 16/32-bit DSP with three-way ILP (1 \gls{ALU}, 2
                        \glspl{LSU}).
                        The \gls{ALU} supports 16/32-bit MAC of integers and
                        fractional numbers\footnote{For fractional numbers one
                        additional left shift of one bit is necessary.}.
                \end{itemize}
                
                The Tmcu and Tzscale both feature \gls{RISC} architectures, and
                achieve similar scores in CoreMark.
                However, the Tzscale consumes less area and uses a popular
                open-source instruction set, so the Tzscale is preferred to the
                Tmcu.

                \subsubsection{Tzscale}
                    The Tzscale is an implementation of the RV32I base integer
                    instruction set.
                    As the RISC-V \gls{ISA} does not define the pipeline
                    architecture, Synopsys modelled the processor with a
                    three-stage pipeline similar to the
                    Z-scale \cite{riscv_manual}:
                    \begin{itemize}
                        \item IF, instruction fetch stage. 
                            Instruction is fetched from the instruction memory.
                        \item DE, instruction decode and execution stage.
                            Operands are loaded from the register file, the
                            \gls{ALU}, shifter, and multiplier execute their
                            operations.
                            Addresses and data are sent to the data memory for
                            load/store operations.
                            Control flow operations are also executed in this
                            stage.
                        \item WB, write back stage. 
                            Results from \glspl{FU} from the DE stage and data
                            from load instructions are written back to the
                            register file.
                    \end{itemize}
                    The results from the operations in the DE stage are
                    written back one stage later, which introduces data
                    hazards.
                    To prevent this data dependency hazard, the operands for
                    the operations in the DE stage can bypass the register and
                    fetch directly from the WB stage.
                    One hazard that can not be circumvented by bypasses is
                    introduced by the load instruction, of which the data is
                    only available in the WB stage.
                    For this hazard stalls will need to be used.

                    The only register file used in the Tzscale is the
                    general-purpose register (\t{R} in
                    Figure~\ref{fig:tzscale_arch}), which is 32x32-bits, with
                    the first register hardwired to 0.
                    The other user-visible register holds the program counter.

                    The data path of the Tzscale is shown in
                    Figure~\ref{fig:tzscale_arch}.
                    The registers and memories related to instruction
                    fetching/decoding and control are not shown.
                    \begin{figure}[H]
                        \centering
                        \includegraphics[width=0.7\textwidth]{figures/tzscale_proc.pdf}
                        \caption{The data path of the Tzscale core (source
                             \cite{tzscale}).}
                        \label{fig:tzscale_arch}
                    \end{figure}

                    As can be seen in Figure~\ref{fig:rv32i}, the RV32I
                    instruction set defines four instruction types (R, I, S,
                    and U), which are all 32-bits wide.
                    The source (\t{rs1} and \t{rs2}) and destination (\t{rd})
                    are always in the same place in each instruction type to
                    simplify decoding.
                    The encoding of immediate values has the same consistency,
                    as they are always sign-extended and packed to the leftmost
                    bit in the field.
                    The design focus of the \gls{ISA} is hardware simplicity,
                    as is typical for \gls{RISC}.

                    The Tzscale also supports the use of the ``C'' extension
                    for instruction compression, which can reduce a RV32I
                    instruction to 16-bits under certain conditions.
                    
                    \begin{figure}[H]
                        \centering
                        \includegraphics[width=0.7\textwidth]{figures/rv32i.pdf}
                        \caption{The instruction types in the RV32I RISC-V
                        instruction set (source  \cite{riscv_manual}).}
                        \label{fig:rv32i}
                    \end{figure}

                \subsubsection{Tvliw}
                    The Tvliw is a \gls{VLIW} processor with two \glspl{ALU}
                    and a three stage pipeline, with the following stages:
                    \begin{itemize}
                        \item IF, instruction fetch stage. The instruction is fetched
                            from the instruction memory.
                        \item ID, instruction decode stage. The instruction is
                            decoded and any addresses for load or store
                            operations are sent to the data memory.
                        \item EX, execution stage. All arithmetic and move
                            operations are executed.
                    \end{itemize}

                    The \glspl{FU} in the VLIW are:
                    \begin{itemize}
                        \item \t{DU0} and \t{DU1}, two \glspl{ALU} for arithmetic on
                            data
                        \item \t{AU0} and \t{AU1}, two units to execute operations on
                            addresses
                    \end{itemize}
                    The \glspl{ALU}, \t{DU0} and \t{DU1}, support addition,
                    substraction, multiplication, bitwise AND, OR, and
                    exclusive-OR, shift operations, and compare operations.

                    Next to the program counter, stack pointer, link register,
                    and single bit condition register, the Tvliw contains three
                    register files:
                    \begin{itemize}
                        \item \t{R}, general-purpose data register which can be
                            accessed by both \glspl{ALU} and both ports on the
                            data memory.
                        \item \t{P}, which holds addresses
                        \item \t{M}, which holds register address modifiers
                    \end{itemize}
                    All registers and \glspl{FU} have a data width of 32-bit.
                    
                    The Tvliw has a 64-bit wide instruction memory (\t{PM})
                    with a 32-bit address bus and a separate 32-bit data memory
                    (\t{DM}) with two read and write ports.

                    The data path of the Tvliw is shown in
                    Figure~\ref{fig:tvliw_arch}.
                    The registers and memories related to instruction
                    fetching/decoding and control are not shown.

                    \begin{figure}[H]
                        \centering
                        \includegraphics[width=0.7\textwidth]{figures/tvliw_proc.pdf}
                        \caption{The data path of the Tvliw core (source
                             \cite{tvliw}).}
                        \label{fig:tvliw_arch}
                    \end{figure}

                    The Tvliw has two instruction formats, where it issues
                    either two or four instructions.
                    The first bit of the instruction indicates the format.
                    For the four-slot \gls{VLIW} instruction, two arithmetic
                    operations on the \t{DU0} and \t{DU1} \glspl{ALU} can be
                    executed, and two load, store, or register moves can be
                    executed at the same time.
                    For the load and store instructions the addresses will be
                    calculated by \t{AU0} and \t{AU1}.

                \subsubsection{Tdsp}
                    The Tdsp is a \gls{DSP} with a three stage pipeline, with
                    the following stages:
                    \begin{itemize}
                        \item IF, instruction fetch
                        \item ID, instruction decode
                        \item EX, execution
                    \end{itemize}
                    The \glspl{FU} of the Tdsp consist of:
                    \begin{itemize}
                        \item \t{alu}, a 32-bit general-purpose \gls{ALU}
                        \item \t{mpy}, a 16x16 multiplier
                        \item \t{sh}, a 32-bit shift unit
                        \item \t{norm}, a 32-bit norm unit, which is used to
                            compute the shift factor for the shift unit to
                            normalise the input
                        \item \t{agu1}, \t{agu2}, address generation units
                        \item \t{xa}, \t{xr}, \t{xs}, \t{xt}, sign extension or
                            half-word extraction units
                        \item \t{cnd}, the implementation of the calculation of
                            the conditional jump bit
                        \item \t{lp_incr}, the hardware loop control index
                            update unit
                    \end{itemize}
                    The Tdsp uses the following registers:
                    \begin{itemize}
                        \item \t{R}, 8x16-bit general purpose register file
                        \item \t{L}, 4x32-bit long word storage
                        \item \t{A}, 8x20-bit for holding addresses
                        \item \t{M}, 4x16-bit for holding address modifiers
                        \item \t{SP}, stack pointer register, to store the
                            address of the top of the stack
                        \item \t{LR}, link register to store the return address
                            of the caller function
                        \item \t{BS}, \t{BL}, registers for holding the start
                            address and loop count for circular buffers
                        \item \t{LS}, \t{LE}, \t{LC}, \t{LP}, registers for
                            loop control
                        \item \t{PC}, program counter
                        \item \t{SR}, status register which holds the \t{CND}
                            conditional bit, \t{IE} interrupt enable bit, and
                            \t{IMSK} interrupt mask
                        \item \t{ISR}, \t{ILR}, interrupt registers
                    \end{itemize}
                    Finally, the memories Tdsp uses are \t{DM} and \t{PM}, the
                    data and instruction (program) memory.
                    \t{DM} can be accessed for words and long words (aliases
                    \t{DMw} and \t{DMl}).

                    The data path of the Tdsp processor is shown in
                    Figure~\ref{fig:tdsp_arch}.
                    The registers and memories related to instruction
                    fetching/decoding and control are not shown.

                    \begin{figure}[H]
                        \centering
                        \includegraphics[angle=270,width=\textwidth]{figures/tdsp_proc.pdf}
                        \caption{The data path of the Tdsp core (source  \cite{tdsp}).}
                        \label{fig:tdsp_arch}
                    \end{figure}
                    
                    The Tdsp's unique data path is accompanied by its unique
                    instruction set.
                    The Tdsp supports 16 and 32-bit variable-length
                    instructions.
                    The 16-bit instructions can be one of the following:
                    \begin{itemize}
                        \item Arithmetic on the \t{R} or \t{L} register, or
                            multiply-accumulate arithmetic
                        \item Load/store instructions
                        \item Move instructions
                        \item Control instructions
                    \end{itemize}
                    32-bit instructions consist of the following categories:
                    \begin{itemize}
                        \item A combination of a 16-bit arithmetic and a control
                            instruction
                        \item A combination of a 16-bit arithmetic and a move
                            instruction
                        \item A combination of a control and a move
                            instruction
                        \item Long control or load/store instructions, which use direct
                            addressing
                        \item Long immediate instructions
                        \item A \gls{DSP} instruction combining a
                            multiply-accumulate instruction with a load/store
                            instruction and a second load instruction
                    \end{itemize}

                    The combination of the hardware loop control, circular
                    buffer registers, dedicated shifter, and
                    multiply-accumulate functionality is advantageous for
                    signal-processing applications and makes the Tdsp a good
                    example of a \gls{DSP}.

            \subsection{TCE}
                With \gls{TCE}, there are unlimited possibilities in designing a
                suitable \gls{TTA} core for \glspl{IMD}. 
                However, designing a new \gls{TTA} processor would be out of scope
                for this thesis.
                Therefore, existing implementations will be assessed.

                Modern \gls{TTA} cores designed in \gls{TCE} are the
                LoTTA \cite{lotta} and its derivatives the PeLoTTA and Super
                LoTTA, which are designed for always-on applications.
                Where the PeLoTTA still maintains the fast branching
                functionality and the energy-efficient operation, the Super
                LoTTA was designed for maximum clock frequency.
                The PeLoTTA also has the overall smallest code size and
                features an improved \gls{IRF}.
                Because of the energy efficient design target of the PeLoTTA,
                this will be the only evaluated core for \gls{TCE}.

                The architecture of the PeLoTTA core can be seen in
                Figure~\ref{fig:pelotta}.
                The PeLoTTA contains three transport buses which connect the
                following \glspl{FU}:
                \begin{itemize}
                    \item an \gls{LSU} for moving data between the buses and
                        the memory
                    \item an \gls{ALU} for basic arithmetic, binary operations,
                        shifting, min/max operations, and conditional
                        operations
                    \item a 32x32-bit register file
                    \item an immediate unit, for loading and storing immediates
                    \item a control unit for executing jumps and branches
                    \item a dedicated IO unit for standard output
                \end{itemize}

                \begin{figure}[H]
                    \centering
                    \includegraphics[width=\textwidth]{figures/pelotta.pdf}
                    \caption{The PeLoTTA core as shown in \gls{ProDe} with its
                        buses and \glspl{FU}.}
                    \label{fig:pelotta}
                \end{figure}

            \subsubsection{Implementation comparison}
                The selection of \gls{ASIP} implementations is dependent on the
                available models from the used toolsets and the architectures
                of interest.
                From the available \gls{TTA} cores only the PeLoTTA is
                selected, providing improvements over the LoTTA and retaining
                energy-efficient operation.
                Representing conventional general-purpose \gls{RISC}
                processors, ASIP Designer provides both the Tmcu and the
                Tzscale.
                Because of the prevalence of signal-processing tasks in
                \glspl{IMD} and the relatively low effort of implementing
                another core in ASIP Designer, the Tdsp will also be
                implemented.
                To bridge the architectural gap between \glspl{TTA} and the
                \glspl{MCU}, a \gls{VLIW} core will also be selected.
                The only \gls{VLIW} core available from ASIP Designer is the
                Tvliw.
                An overview of all selected \glspl{ASIP} with their properties
                can be found in Table~\ref{tab:coresel}.
                \input{tables/coresel.tex}

                For the PeLoTTA, Tzscale, and Tdsp synthesis results are also
                available, and are listed in Table~\ref{tab:asip_cores}.
                Table~\ref{tab:asip_cores} shows that the PeLoTTA and Tzscale
                are comparable in terms of core area and clock frequency.
                The Tdsp reaches only half of the maximum clock frequency of
                the Tzscale and PeLoTTA and uses $20-32\%$ more area.
                The Tvliw likely uses the most area due to its high
                issue width, but no synthesis results are available for that
                core.
                
                \input{tables/asip_cores.tex}

    \section{Benchmarks}
        Multiple benchmarks exist for evaluating the performance of an embedded
        system.
        Some benchmark suites will be presented in order to select the
        benchmarks that will represent the workloads of \glspl{IMD}.

        \subsection{Requirements}
            The benchmarks should be representative of typical workloads for
            \glspl{IMD}.
            Typical workloads can be broken down into six categories:
            \begin{itemize}
                \item Control systems; state machines, signal processing.
                \item Encryption; communication should be encrypted
                \item Data compression; if the \gls{IMD} uses wireless
                    communication to transfer data, every bit that can be
                    compressed reduces the energy necessary for that transfer.
                \item Error detection/correction; transferred data should be
                    able to be verified .
                \item \Gls{ANN}; a modern solution to pattern recognition and
                    signal processing.
            \end{itemize}
            Next to a representative collection of benchmarks, there are some
            requirements related to the implementation of these benchmarks:
            \begin{itemize}
                \item Free and open-source license
                \item Able to run without any OS (bare bones)
                \item Architecture-agnostic
                \item Small memory footprint
            \end{itemize}

        \subsection{EEMBC benchmarks}
            \begin{itemize}
                \item CoreMark \cite{eembccoremark}:
                    The industry-standard benchmark for microcontrollers is
                    \gls{EEMBC}'s CoreMark.
                    It focusses on list processing, matrix manipulation, state
                    machines, and error detection (CRC16). 
                    The source code for CoreMark can be found on GitHub
                    \cite{coremarkgit}.
                \item ULPMark \cite{ulpmark}:
                    ULPMark is also developed by \gls{EEMBC}, based on
                    CoreMark, with a focus on \gls{ULP} devices.
                    ULPMark comes in three flavours:
                    \begin{itemize}
                        \item ULPMark-CoreProfile \cite{ulpmarkcp}, which
                            sleeps after executing a workload, combining power
                            measurements for sleep and active modes.
                        \item ULPMark-PeripheralProfile \cite{ulpmarkpp}, which
                            assesses the performance of peripherals.
                        \item ULPMark-CoreMark \cite{ulpmarkcm}, which uses
                            CoreMark as the active workload and an external
                            power measurement board to determine the CoreMark
                            executions per Joule.
                    \end{itemize}
                    ULPMark-PeripheralProfile and ULPMark-CoreProfile will not
                    be discussed because of the requirement of external modules
                    (either peripherals or power measurement boards).
                \item SecureMark-TLS \cite{securemark}:
                    SecureMark-TLS implements TLS using ECDSA, SHA256, and
                    SHA128.
            \end{itemize}
            \Gls{EEMBC} also provides benchmarks for machine learning; MLMark
            \cite{mlmark} and ULPMark-ML \cite{ulpmarkml}.
            However, MLMark only runs on architectures with a GPU, and
            ULPMark-ML is still in development at the time of writing.

            Another honourable mention is IoTMark, which focusses on wireless
            communication and reading out sensors.
            This benchmark is not listed because it requires a \gls{SoC} with a
            Bluetooth radio.
            
        \subsection{MiBench}
            MiBench \cite{mibench} is a large collection of 35 benchmarks.
            MiBench mainly focusses on consumer PC workloads and provides
            benchmarks chosen for six categories:
            \begin{itemize}
                \item Automotive/industrial control systems
                \item Consumer
                \item Office
                \item Network
                \item Security
                \item Telecommunications
            \end{itemize}
            MiBench was presented in 2001 as a free, open-source alternative to
            \gls{EEMBC}'s benchmarks (although CoreMark and other \gls{EEMBC}
            benchmarks are now open-source as well).

        \subsection{SPEC2017}
            The Standard Performance Evaluation Corporation released their
            first benchmark in 1999, the SPEC2000.
            SPEC2017 is the third and latest release of the SPEC CPU benchmark
            suite, and contains 43 benchmarks.
            SPEC2017 is designed for heavy integer and floating point workloads
            such as benchmarks for fluid dynamics and weather forecasts, and
            thus is unsuited for embedded systems.

        \subsection{ImpBench}
            ImpBench \cite{impbench} is a benchmark suite
            specifically designed for \glspl{IMD}.
            The original release of ImpBench provides eight benchmarks for four
            categories:
            \begin{itemize}
                \item Compression
                \item Encryption
                \item Data integrity
                \item Real applications
            \end{itemize}
            In the revised version of ImpBench \cite{impbench2} introduces
            ``stressmark'' versions of the two benchmarks for real applications
            in order to improve simulation time.

        \subsection{Solutions comparison}
            Table~\ref{tab:benchmarks} shows which benchmarks from the
            benchmark suites are suited for the required categories.
            Table~\ref{tab:benchmarks2} shows the licence and architectural
            requirements.
            Some notes about the tables:
            \begin{itemize}
                \item In Table~\ref{tab:benchmarks2} the requirement for
                    machine learning is missing, as none of the benchmarks
                    listed in that table have a focus on machine learning.
                \item The compression algorithm that MiBench possesses
                    (\t{jpeg}) is lossy and designed for images, and thus is
                    not representative for a compression algorithm for
                    \glspl{IMD}.
                    Thus it is not listed in Table~\ref{tab:benchmarks}.
            \end{itemize}
            
            \input{tables/benchmarks.tex}
            \input{tables/benchmarks2.tex}

            It comes as no surprise that the benchmark suite designed for
            \glspl{IMD} has the best match for the requirements, although some
            work will be required to make the benchmarks run on the different
            architectures.
            ImpBench lacks a good benchmark for assessing typical control
            systems, other than the DMU benchmark.
            ULPMark-CoreMark is not an option;
            even though it can be used with a university licence, the benchmark
            relies heavily on letting the processor sleep, which is not
            possible for any of the selected implementations.
            Therefore, CoreMark will also be selected to run on the \gls{ASIP}
            implementations.

            As for a benchmark to assess performance for \glspl{ANN}, no
            portable, lightweight benchmarks are available.
            Therefore, a custom benchmark will be designed and implemented in
            the next chapter.
        
            Section~\ref{sec:nn_in_imds} shows that most \glspl{ANN} in
            \glspl{IMD} are either \glspl{CNN} or \glspl{RNN}.
            The main difference between these networks is that a \gls{CNN} is a
            feed-forward network (output of a neuron is only dependent on the
            input), whereas a neuron in an \gls{RNN} has local memory to which
            data can be fed back from the outputs of other neurons.

            For a benchmark, a \gls{CNN} for digit classification of the MNIST
            dataset \cite{mnist} will be implemented.
            Even though the data does not represent biological signals, the
            dataset is very well-known and \glspl{CNN} have been used
            extensively to classify the digits in the set \cite{mnist}.

        
