\chapter{Introduction}\label{sec:intro}
    % Embedded computing, what is it? how is it developing over the years?
    % How did embedded computing first come to pass?
    % were they ASIC at first?
    % how did general purpose MCU and stuff make their way in it?
    % what about the medical world?
    Ever since the success of the first \gls{MOSFET}-based computers in the late
    1950's, they reduced in both size and power following the famous Moore's
    Law.
    This development has enabled computers to transition from the initially
    massive, immovable machines to the portable devices we now use in our daily
    lives.
    One branch of computing has flourished exceptionally due these
    developments; embedded systems.

    Embedded systems have started out simple control systems like in the Apollo
    project.
    As their performance increased and their size and power consumption
    diminished, embedded systems have become less constrained and are now
    closer resembling traditional computers.
    Application fields which at first were exclusive to \glspl{ASIC} or
    specialised \glspl{DSP} are now largely dominated by \glspl{GPP} running
    Linux or Android.
    One field which is not as quickly to adapt modern advancements is medical
    implants.
    This is not necessarily because the medical sector is slower to adapt new
    technologies compared to other sectors, but more due to the fact that
    implantable devices have exceptionally strict constraints.
    The factor that constrains implantable devices the most is power usage.  As
    the implant must be small in size, there is limited volume available for
    the battery to store energy in, and charging or replacing batteries is an
    activity that is desired to be infrequent.
    Although current solutions in implantable devices do employ the use of
    general-purpose \glspl{MCU}, there is much to gain with a more specialised
    approach.
    
    \section{Context}
        % Context (what is the project, where is it being carried out, how will
        % this research have impact on the world)
        In 1991 the department of Electrical Engineering of the TU Delft
        used \glspl{TTA} to design application-specific processors as part
        of the MOVE framework \cite{corporaal1991move}.
        In contrast to common architectures like RISC, \gls{TTA} processors
        expose the data path of the processor to the programmer.
        This enables \gls{TTA} processors to minimise the amount of data
        transfers and thus improve the energy efficiency of the processor.
        This property makes \glspl{TTA} interesting for \glspl{IMD}.
        The MOVE framework was the inspiration for
        \gls{TCE} \cite{TCEToolset,move}, developed by the Customized
        Parallel Computing group of Tampere University.
        \Gls{TCE} is a toolset which both the hardware and software for
        specialised \gls{TTA} processors can be developed.
        The instruction set, data path, and \glspl{FU} of the processors can be
        designed with the toolset, which makes \gls{TCE} a toolset for
        \gls{ASIP} design \cite{openasip}.


        Meanwhile, Erasmus Medical Centre investigates low-power processor
        architectures for deployment in medical devices.
        They are looking for new processor architectures that fulfill their
        computational and power requirements, which creates an opportunity for
        an application of \glspl{ASIP}.
        To assess performance of implantable processors, ImpBench
        \cite{impbench,impbench2} was developed.
        ImpBench is a benchmark suite designed specifically to assess a
        processor's ability to handle implant-type workloads.
        
        This thesis will assess the performance of computer architectures for
        \glspl{ASIP} for use in \glspl{IMD}.
        One of the architectures assessed is \gls{TTA}.
        The aim of this research is to provide insight on the choice of
        base architecture for \glspl{ASIP} in \glspl{IMD}.
    
    \section{Challenges}
        % Challenges (what is the general problem and what are the possible
        % challenges to solving it)
        The problem in \glspl{IMD} is the constant need for processors cores
        with higher power efficiency, in order to decrease the frequency of
        surgical operations.
        The use of \glspl{ASIP} may provide a solution to this problem.
        
        In order to assess the performance of \gls{ASIP} architectures for
        medical implantable processors, a proper experimental setup needs to be
        created which can accommodate for the different \gls{ASIP} architectures.
        The implementations for the selected \gls{ASIP} architectures should be
        suitable for small \gls{ULP} processors.
        With the implementations selected, the performance of the processors
        can be compared using benchmarks to resemble the workload of
        \glspl{IMD}.
        A comparison such as this is highly prone to pitfalls that may heavily
        influence the results of the research.
        The choice of architectures, their implementations, the simulated
        workloads running on these implementations, and the extraction of power
        and performance figures is non-trivial and requires insight in multiple
        levels of computer architecture.
    
    \section{Problem statement}
        % Problem statement and research questions
        The core question that arises is:
        \textit{How do different \gls{ASIP} computer architectures compare for
        medical implantable devices?}
        The following sub-questions arise from the problem statement:
        \begin{itemize}
            \item \textit{Which benchmarks need to be run to represent the
                workloads of \glspl{IMD}?}\\
                The workloads of \glspl{IMD} need to be identified, and
                suitable benchmarks that represent these workloads need to be
                selected.
            \item \textit{Which processor architectures are currently prevalent
                in \glspl{IMD}?}\\
                The state of the art in processor architectures of
                processor-based \glspl{IMD} will be reviewed in to provide
                insight in the selection of \gls{ASIP} architectures.
            \item \textit{Which low-power implementations of computer
                architectures are suitable for \glspl{ASIP}?}\\
                A selection of \gls{ASIP} implementations of the relevant
                architectures will be made.
                The benchmarks that represent the \gls{IMD} workloads will
                run on these implementations, of which the performance can 
                be measured.
        \end{itemize}

        In order to answer the question that lies at the core of the problem,
        the sub-questions listed above will first be answered in the next
        chapter.
    
    \section{Thesis outline}
        To start off, some background information and related work
        (Chapter~\ref{sec:background}) will be provided about \glspl{IMD}, the
        competing processor architectures, and design and development of these
        architectures.
        The state of the art of \glspl{IMD} will be assessed, which will
        provide insight on the architectures used and the workloads which they
        run.
        This background knowledge will provide the reasoning behind the
        selection of architectures, their implementations, and benchmarks used
        to simulate the workloads in Chapter~\ref{sec:altsol}.
        The selected processor implementations and benchmarks will be
        implemented in Chapter~\ref{sec:implementation}, providing insight in
        the design choices made in this process.

        In the Results chapter (Chapter~\ref{sec:results}) the 
        setup will be explained for simulations and power measurements, the
        resulting figures will be presented and discussed.
        The thesis will conclude in Chapter~\ref{sec:conclusion}, which will
        summarise the findings in this thesis, formulate a concise answer on
        the research question, and provide recommendations for future work.
