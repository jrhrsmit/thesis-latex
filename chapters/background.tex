\chapter{Background}\label{sec:background}
    % Based on a detailed analysis of the problem, discuss the various concepts 
    % related to the project that need more in-depth knowledge to understand the 
    % solution.

    % So that's _relevant_ background info! not history and stuff

        % 1. There exists a need for lower-power ASIPs in implants
        % 2. TTA has proven to be a better job at ULP DSP workloads than VLIW and
        %    traditional ISAs
        % 3. Parallelism leads to same performance at lower frequency
    To get a better understanding of the problem, relevant topics for this
    thesis will be discussed first.
    The application domain of \glspl{IMD} will be described, as well as their
    characterising workloads.
    Some required background for the implementation regarding \glspl{ASIP},
    \gls{ASIP} development, and \glspl{TTA} will follow, concluding with
    information about benchmarks and related work.

    \section{Processor-based implantable medical devices}
        An \gls{IMD} is an active medical device used on humans or other
        animals for diagnostic, drug administration, stimulation, actuation, or
        other medical purposes.
        In the past, the majority of \glspl{IMD} have been based on
        \glspl{ASIC} to comply with strict reliability and power requirements.
        Advancements in silicon process technology have relaxed the power and
        size constraints and enabled the use of a more generalised, structured
        design approach \cite{impbench} using off-the-shelf microcontrollers.
        % Moving to a more generalised design calls for reusable hardware
        % designs, especially retargetable and extendable microprocessors, along
        % with using higher abstraction levels in software (C/C++).
        % Therefore, it comes as no surprise that the shift from \glspl{ASIC} to
        % \glspl{MCU} is a growing trend \cite{impbench}.

        \Glspl{IMD} come in many shapes and applications.
        Implantable devices which are or could be equipped with an embedded
        processor can be grouped into two categories
        \cite{strydis2011universal}:
        \begin{enumerate}
            \item Monitoring of physiological data, for example:
                \begin{itemize}
                    \item Blood monitoring \cite{blood1}
                    \item Tissue bio-impedance \cite{bioimpedance1}
                    \item Neuron recording \cite{neuron1}
                    \item Brain imaging \cite{brain1}
                    \item Brain interfacing \cite{brain2}
                    \item Seizure detection \cite{seizure1}
                \end{itemize}
            \item Stimulation or actuation of tissue, for example:
                \begin{itemize}
                    \item Neurostimulation \cite{yearwood2016peripheral}
                    \item Neuromodulation \cite{bemelmans1999neuromodulation}
                    \item Vestibular prosthesis \cite{vestibular1}
                    \item Cortical control \cite{cortical1,cortical2}
                    \item Pacemakers \cite{pacemaker1}
                    \item Implantable defibrillators \cite{defib1}
                    \item Corneal stimulation \cite{cornea1}
                    \item Bladder control \cite{bladder1}
                    \item Cochlear implants \cite{cochlear1,cochlear2}
                \end{itemize}
        \end{enumerate}
        These \glspl{IMD} typically acquire data in some form, and have an
        optional signal-processing task for actuation.

        The batteries for the \gls{IMD} are usually limited to 1-3Ah
        \cite{siddiqi2019imd}.
        For \glspl{IMD} without actuators, the battery capacity in combination
        with a minimal lifetime of the \gls{IMD} of 5 years forces the average
        working current of the device down to $23-65\si{\micro\ampere}$
        \cite{batterylife}.
        Thus generally all \glspl{IMD} are optimised for low power.
        Many of these devices could be based on the same low power processor
        core, with the only variable being the actuator or sensor.
        For devices which do sink large currents in actuators like
        neurostimulators, the power consumption is less important as the
        battery has to be charged frequently.

    \section{Relevant processor architectures}
        In order to provide a better understanding for processor architectures
        prevalent in \glspl{IMD} and their alternatives, relevant processor
        architectures will be discussed in this section.

        \subsection{RISC}
            In contrast to a \gls{CISC}, where the focus lies on reducing
            instruction count, a \gls{RISC} is focused on reducing the
            complexity of the instruction set.
            Reducing complexity in the instruction set is not only about
            reducing the number of instructions in the set, but also reducing
            the complexity of the hardware required to decode and execute the
            instructions.
            Although this does automatically lead to more instructions being
            executed, the reduced hardware complexity allows for higher clock
            frequencies and less area usage than a \gls{CISC}.

            \gls{RISC} architectures are often load-store architectures, where
            the load and store instructions copy data between memory and
            general-purpose registers, and all arithmetic is executed on the
            registers.
            Another trait to achieve simplicity is fixing the length of the
            instructions and the locations of the operators and data in the
            instructions.
            
            Because of the small area, \gls{RISC} is popular amongst
            microcontrollers, and thus is also prevalent in \glspl{IMD}.
            Popular modern implementations of \gls{RISC} \glspl{ISA} are ARMv6,
            ARMv7, and RISC-V.

        \subsection{DSP}
            \glspl{DSP} are microprocessors with specialised architectures for
            signal-processing workloads, like filters.
            A typical \gls{DSP} instruction is the multiply-accumulate
            instruction, which can perform a multiplication and addition in one
            instruction.
            This operation is heavily used in typical \gls{DSP} workloads, like
            Fourier transforms.
            As signal-processing workloads often require processing large
            amounts of data, a \gls{DSP} typically allows for loading multiple
            data from memory in one instruction.
            Some implantable devices that focus on a heavy signal-processing
            task, like hearing aids or neurological analysis, feature a
            \gls{DSP} \cite{mcu12, marsman2006dsp}.

        \subsection{VLIW}
            \gls{VLIW} processors are characterised by their long instruction
            word which can issue instructions to multiple \glspl{FU} in the
            processor at once.
            The instruction word contains multiple issue slots in which
            \gls{RISC}-like instructions are placed, which allows a \gls{VLIW}
            core to exploit \gls{ILP} by design.
            This makes them also suitable for \gls{DSP} applications.
            An example of a \gls{VLIW} \gls{DSP} is the C6000 series of Texas
            Instruments \cite{c6000ti}.
            % Although there are very few \gls{VLIW} processors used in
            % \glspl{IMD}, the architecture will be relevant in this thesis.

        \subsection{Transport-triggered architectures}
            A \gls{TTA} is a one-instruction set \gls{VLIW} architecture.
            As opposed to traditional architectures where the instruction
            specifies the operation, a \gls{TTA} instruction defines the
            movement of data over the transport buses in the processor.
            The \glspl{FU} do not have direct access to the register file, as
            it is also only accessed via the buses.
            This reduces the complexity of the register file, and reduces the
            traffic through the register file
            \cite{corporaal1997microprocessor}.
            
            Operations are executed as a consequence of the movement of data
            over the buses, and are triggered by writing to a specific port for
            each \gls{FU}.
            As opposed to \gls{DSP}, \gls{VLIW}, and \gls{RISC} architectures,
            the result of a \gls{FU} is not written to the register file, but
            instead remains in the register at the output of that \gls{FU}
            until it is moved.
            The exposed internal data path allows for explicitly bypassing the
            register file, but may also require more instructions per
            operation.
            For example, \mintinline[bgcolor=bg]{C}{r3 = r0 + (r1 << r2)} in a
            \gls{RISC} \gls{ISA} could be:
            \begin{minted}[bgcolor=bg,autogobble]{asm}
                slli r1, r1, r2
                add r3, r0, r1
            \end{minted}
            Whereas for a \gls{TTA} (with a shifter and adder) it would become:
            \begin{minted}[bgcolor=bg,autogobble]{asm}
                move r1, sll.in0
                move r2, sll.in1t
                move r0, add.in0
                move sll.out, add.in1t
                move add.out, r3
            \end{minted}
            Here \t{sll} is the shifter and \t{add} is the adder, and the \t{-t}
            postfix indicates the port that triggers the operation.

            A \gls{TTA} can contain multiple buses, each of which can be
            connected to a subset of all \glspl{FU}.
            Each bus is then typically controlled by their own slot in the
            instruction word.
            This also implies that multiple moves can be executed in a single
            instruction, thus providing instruction-level parallelism.
            Because the data path is so exposed and every movement of data is
            specified by the instructions, the compiler has more control over
            the processor and is able to optimise more compared to compilers
            for traditional architectures at the cost of added complexity.

            The downside of a \gls{TTA} is that context switching (and thus
            interrupts) are very hard to implement. 
            This is because the state of the processor is not only described by
            the register file but also by the current state of each \gls{FU}.
            The lack of context switching renders the \gls{TTA} unsuitable for
            multithreaded or interrupt-driven applications, but it remains
            suited for always-on applications \cite{openasip_tta}.

            \Glspl{TTA} have mostly been implemented as solutions for low-power
            systems with accelerators for neural networks \cite{aivotta},
            Fourier transforms \cite{ttafft}, audio processing \cite{ttaaudio},
            but also for low-power signal-processing \cite{ttafft} and
            always-on \cite{lotta} applications.

    \section{ASIP}
        An \Gls{ASIP} is the trade-off between \glspl{ASIC} and
        general-purpose processors \cite{liem1994instruction}.
        \Glspl{ASIP} are usually small processors with a tailored instruction
        set for the application and are usually deployed in an environment that
        forces such requirements \cite{fisher_faraboschi_young_2005}.
        This tailored \gls{ISA} provides greater computational
        efficiency and requires less power for the specified workload.

        \Glspl{ASIP} may be implemented by means of extending an existing/basic
        \gls{ISA} with specialised instructions, or by adding reconfigurable
        hardware in the processor \cite{keutzer2002asic}.

        \Glspl{ASIP} can be deployed as standalone application-specific
        processors like \glspl{DSP}, but are also prevalent in \glspl{SoC},
        where they fulfill roles as programmable accelerators.

        \begin{figure}
            \centering
            \includegraphics[width=0.4\textwidth]{figures/asip.pdf}
            \caption{A visualisation of the trade-off of \glspl{ASIP} in terms
                of flexibility and performance (Source  \cite{synopsysintro}).}
            \label{fig:asip}
        \end{figure}

        \subsection*{ASIP development}
            Because the compiler for the \gls{ASIP} is dependent on the
            \gls{ISA} and hardware design of the \gls{ASIP}, \glspl{ASIP} are
            developed in a so-called co-design environment in which both the
            hardware and software for the \glspl{ASIP} are developed.
            This is the most significant difference in development for
            \glspl{ASIP} compared to \glspl{GPP}, as the \gls{ISA} and its
            implementation are not part of the designs space when developing
            for a \gls{GPP}.

            The ``leading tool solution'' \cite{asipdesignerweb} in the industry
            is Synopsys ASIP Designer, and provides a highly automated
            workflow.
            An open-source alternative is the \gls{TTA}-based co-design
            environment (\gls{TCE}), developed by the customized parallel
            computing research group of Tampere University.

            \subsubsection{ASIP Designer}
                ASIP Designer is a tool suite developed by
                Synopsys \cite{asipdesignerweb}.
                ASIP Designer focusses on a wide range of \glspl{ASIP}, from
                extendible processors to programmable data path \glspl{ASIP}.

                Processors are described using ASIP Designer's proprietary
                language nML.
                nML is a C++ style language in which both the structure
                (storages, \glspl{FU}, connections) of the processor and the
                instruction set are described.
                An example of nML is shown in Listing~\ref{lst:nml}.
                Another proprietary language PDG, which is based on C, then
                describes the behaviour of the \glspl{FU}, IO interfaces, and
                processor control unit.
                An example of PDG is shown in Listing~\ref{lst:pdg}.

                With nML and PDG, the entire ASIP is described, and ASIP
                Designer can generate an \gls{SDK} including C/C++
                compiler (called Chess), and debugger/simulator (Checkers)
                based on that model.
                The \gls{HDL} description of the hardware can also be directly
                generated from the processor model using Go in both Verilog and
                \gls{VHDL}.
                The Chess compiler supports the LLVM front-end Clang, which
                can provide more aggressive optimisation.
                Along with the program written in C or C++, the generated
                simulator can produce the memory contents for all defined
                memories in the processor.

                As shown in Figure~\ref{fig:adworkflow}, the development cycle
                in ASIP Designer is first focused on designing the algorithm
                and the processor model.
                The processor model, which includes the description of the
                instruction set, is used to generate a semi-custom SDK for the
                processor.
                The SDK is then used to implement and test the algorithm, and
                the simulations in the SDK provide insight for refinement for
                the processor model.
                To assist in this process, Checkers provides an overview of all
                registers and memories in the processor and allows for stepping
                through the source code and single instructions.
                The placement of breakpoints and watchpoints in both the
                assembly and C is also supported.
                After simulation is successful and the hardware/software
                co-design is completed, the \gls{HDL} can be
                generated and exported to other Synopsys tools for
                \gls{ASIC} or \gls{FPGA} synthesis.
                Finally, automatically generated test programs can verify
                the integrity of the processor.
       
                For further understanding of the workings of the tools of ASIP
                Designer, it is recommended to reference the online training
                program \cite{asipdesignerweb} and the ASIP Designer manuals,
                which come with the software.

                \begin{figure}[H]
                    \centering
                    \includegraphics[width=0.9\textwidth]{figures/asip_designer_block_diagram_upscaled.jpg}
                    \caption{Workflow in Synopsys ASIP Designer (source
                     \cite{asipdesignerweb})}
                    \label{fig:adworkflow}
                \end{figure}

                \begin{listing}
                    \begin{minted}[bgcolor=bg,autogobble]{C}
                        word add(word a, word b) {
                            return a + b;
                        }
                    \end{minted}
                    \caption{An example of the behaviour \t{add} instruction as
                        described with PDG.}
                    \label{lst:pdg}
                \end{listing}

                \begin{listing}
                    \begin{minted}[bgcolor=bg,autogobble]{C}
                        enum stage_names {IF,   // Instruction Fetch
                                          DE,   // Instruction Decode and Execute
                                          WB};  // Writeback
                        mem PMb[2**14] <uint8,addr> access {};

                        enum eR { x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15};
                        reg R[16]<w32,uint4> syntax (eR) read(r1 r2) write(w1 wd);
                    \end{minted}
                    \caption{A modified excerpt of an nML processor model of
                        a Z-scale RISC-V processor.
                        The pipeline stage names, program memory, and register
                        file is described.}
                    \label{lst:nml}
                \end{listing}

            \subsubsection{TCE}
                \Gls{TCE} \cite{openasip} provides an open-source \gls{ASIP}
                development toolset for developing \glspl{ASIP} with transport
                triggered architectures.
                \Gls{TCE} is the successor of the MOVE
                project \cite{openasip_move}. 
                
                Figure~\ref{fig:oaworkflow} shows the workflow for designing an
                \gls{ASIP} with \gls{TCE}.
                The designer creates the processor model in the \gls{TTA}
                \gls{ProDe}, in which \glspl{FU} are created and connected by
                transport buses.
                The operations of the \glspl{FU} are selected from the
                operation set abstraction layer database, which contains the
                static properties and behaviours of the operations.
                The behavioural models of the \glspl{FU} are then
                described by a \gls{HDB} file, which contains \gls{HDL}
                descriptions of the blocks.
                The links between the \gls{ADF} and \gls{HDB} file are
                described in the \gls{IDF} file.
                Figure~\ref{fig:prode} shows ProDe with a minimal \gls{TTA}
                using one bus connected to some \glspl{FU}.
                \Gls{ProDe} describes the processor model in an XML file called
                the \gls{ADF}.

                \begin{figure}[H]
                    \centering
                    \includegraphics[width=0.8\textwidth]{figures/prode.png}
                    \caption{A minimal \gls{TTA} architecture (provided by
                        \gls{TCE}) as shown in ProDe.}
                    \label{fig:prode}
                \end{figure}

                The compiler, \t{tcecc}, uses the generated \gls{ADF} to
                compile programs written in a high-level language (C, C++, or
                OpenCL) for the specified architecture using the LLVM front-end
                Clang.
                The designer can then use \t{ttasim} (\gls{CLI}) or \t{proxim}
                (\gls{GUI}) to simulate the program on the processor and
                generate profiling information.
                This information will provide insight for new design iterations
                on the hardware and software.

                Finally, the \gls{ProGe} will be used to generate the hardware
                description of the processor in Verilog or \gls{VHDL}.
                The \gls{ADF}, \gls{IDF}, and compiled binary, provide all
                necessary information for \gls{ProGe} to generate the
                \gls{HDL}.
                
                \Gls{ProGe} also supports generating \gls{HDL} directly for specific
                platforms like the Altera Stratix II device family.
                To generate the memory contents of the processor, the
                command-line utility \t{generatebits} can be used, which
                supports multiple output formats.

                For more information on \gls{TCE} it is recommended to refer to
                the \gls{TCE} homepage \cite{openasip} and the book
                \textit{Computing Platforms for Software-Defined Radio}
                \cite{TCEToolset}.

                \begin{figure}
                    \centering
                    \includegraphics[width=\textwidth]{figures/tce_design_flow.png}
                    \caption{Workflow in \gls{TCE} (source
                     \cite{openasip})}
                    \label{fig:oaworkflow}
                \end{figure}
            \begin{figure}[H]
                \centering
                \includegraphics[width=0.6\textwidth]{figures/rnn.pdf}
                \caption{A simple recurrent neural network architecture for
                    predictions of words, and its time layered representation
                    (Source: \cite{aggarwal2018neural}).
                    $W_{xh}$, $W_{hy}$, and $W_{hh}$ are the weight matrices.
                    $\bar{x}$, $\bar{h}$, and $\bar{y}$ are the input, hidden
                    state, and output vectors.}
                \label{fig:rnn}
            \end{figure}

    \section{Benchmarks}
        The performance\footnote{Performance is defined here as total execution
        time of a defined program on the processor.} of a single-threaded
        processor can be evaluated by using the \textit{Iron Law of
        Performance} \cite{eeckhout2010computer}:
        \begin{equation}
            T = N \cdot CPI \cdot \frac{1}{f}
        \end{equation}
        Here $T$ is the execution time, $N$ is the number of total
        instructions for the program, $CPI$ is the average clocks per
        instruction, and $f$ is the clock frequency.
        
        The maximum clock frequency is only dependent on the processor
        architecture and its implementation, but the $CPI$ and number of
        instructions is dependent on both the architecture and the program.
        Simulations of programs on processors in ASIP Designer and \gls{TCE}
        can provide these figures, but the results may vary wildly per supplied
        program.

        Benchmarks are programs designed for assessing the performance of a
        processor for a specific workload by running algorithms typical for
        that workload.
        Benchmarks typically consist of multiple algorithms that characterise
        the workload.
        Many general-purpose benchmarks exist to compare general-purpose
        processors, even for embedded systems.
        However, when designing a system for a single application (which is
        the case for \glspl{IMD} and embedded systems in general), such
        general-purpose benchmarks may provide a distorted view of the
        performance for the application the system is designed for.
        It follows that the characterisation of the workload and the
        identification of benchmarks that represent that workload is important
        in comparing performance of processors in \glspl{IMD}.

        \subsection{EEMBC}
            The \gls{EEMBC} is a consortium led by many market leaders in the
            field of embedded processors, like Arm, Texas Instruments, ST
            Microelectronics, and Silicon Labs.
            The \gls{EEMBC} aims to develop ``industry-standard benchmarks for the
            hardware and software used in autonomous driving, mobile imaging,
            the Internet of Things, mobile devices, and many other
            applications'' \cite{}.
            Their most famous benchmark CoreMark is indeed the industry
            standard benchmark for embedded microcontrollers, and thereby the
            successor of Dhrystone.
            As \gls{ULP} \glspl{SoC} with integrated wireless
            radios are becoming more relevant for \gls{IoT} applications,
            \gls{EEMBC} has also released benchmarks for those application
            fields, such as IoTMark and ULPMark.

        \subsection{ImpBench}
            ImpBench is a benchmark suite developed specifically to benchmark
            biomedical implantable processors.
            It is the only benchmark specifically developed for \glspl{IMD}.
            ImpBench was proposed in 2008 \cite{impbench}, with a revision in
            2010 \cite{impbench2}.
            The benchmark suite contains eight benchmarks for four workloads;
            compression, error detection, encryption, and synthetic benchmarks
            which simulate real-life applications.
            The following benchmarks are included:
            \begin{itemize}
                \item MiniLZO, a light-weight variant of the LZO
                    high-performance lossless compression library.
                    LZO is in itself a variant of the LZ77 data compression
                    algorithm.
                    It implements zlib (RFC 1950 \cite{zlib}) and Deflate (RFC
                    1951 \cite{deflate}) for compression and decompression.
                    In the benchmark, only compression is used.
                \item Finnish, the Finnish submission for the Dr. Dobbs
                    compression contest in 1991 \cite{compressioncontest}.
                    Also based on the LZ77 algorithm.
                \item MISTY1, a block cipher developed in 1995 (RFC 2994
                    \cite{misty1}) which was recommended by CRYPTREC in 2003.
                    The cipher has been broken in 2015 using integral
                    cryptanalysis.
                \item RC6, a block cipher developed in 1998 with a small code
                    size.
                \item CRC32, a 32-bit cyclic redundancy check algorithm for
                    error detection.
                \item Checksum, a simple checksum algorithm with 32-bit
                    summation and inversion.
                    The algorithm folds the 32-bit sum over into a resulting
                    16-bit checksum.
                \item Motion, a synthetic benchmark which simulates a motion
                    sensor.
                    The benchmark simulates the functionality of a motion
                    sensor by generating a sensor sample, comparing it against
                    a threshold, and then sleeping the CPU by running a
                    while-loop.
                \item DMU, a synthetic benchmark which simulates a drug
                    delivery and monitoring unit based on the work of Cross et
                    al \cite{mcu2}.
            \end{itemize}

    \section{Artificial neural networks}
        \Glspl{ANN} are a form of machine learning based on the learning
        mechanism of biological nervous systems. 
        The similarities lie in the fact that the connections between
        biological neurons can be strengthened by external stimuli, which
        causes the system to learn \cite{aggarwal2018neural}.
        Two types of \glspl{ANN} will be reviewed; \glspl{CNN} and \glspl{RNN}.

        \subsection{Convolutional neural networks}
            \Glspl{CNN} are layered feed-forward networks used frequently in
            computer vision for image classification and object detection.
            \Glspl{CNN} have been the most successful of all types of neural
            networks and outperform humans in image classification
            \cite{aggarwal2018neural}.
            As the name implies, a \gls{CNN} makes use of convolution layers to
            map the outputs of the previous layer onto the input of the next
            layer.
            The output of every node in the layer is then given by convolution
            of the connected outputs of the previous layer and the weights of
            these connections.
            The convolution layers are interleaved with pooling and activation
            layers.
            The activation layer maps each output of the convolution layer on
            its activation function.
            The purpose of the activation function is to map the values from
            the convolution layer into a binary class label.
            The aim of the pooling layer is to reduce the size of the network
            by sub-sampling the outputs of the previous layer.

            A simple \gls{CNN} architecture can be seen in
            Figure~\ref{fig:cnn}, which is designed to classify handwritten
            digits in images.
            
            \begin{figure}[H]
                \centering
                \includegraphics[width=0.9\textwidth]{figures/cnn.pdf}
                \caption{A simple convolutional neural network with one
                    convolution layer for classification of handwritten images
                    in the MNIST dataset (Source: \cite{mnist3lnnblog}).}
                \label{fig:cnn}
            \end{figure}

        \subsection{Recurrent neural networks}
            \Glspl{RNN} are used on sequential data like time series and text.
            To act on the temporal dimension, an \gls{RNN} stores previously
            received samples in a hidden state in its network.
            New samples are then processed using the current state, and the
            current state is updated with the latest sample.
            This way the nodes in the network have two variables; the new
            sample and the internal state of the node.
            When the \gls{RNN} acts on a fixed time window, the \gls{RNN} can
            be unfolded in time.
            This representation is shown in Figure~\ref{fig:rnn}.


    \section{Related work}
        This section presents references to existing background work related to
        processor-based \glspl{IMD}, their workloads, and \glspl{ASIP}.
       
        \subsection{Processor-based IMDs}\label{sec:proc_imds}
            Many \glspl{IMD} using processor cores have been described in
            academic work, of which some will be highlighted.
            \cite{mcu3} and \cite{mcu4} describe implantable devices for
            stimulation of denervated muscles, both controlled by a 16-bit PIC
            with a \gls{RISC} architecture \cite{pic16c5x,pic16f87x}.
            The muscles are periodically stimulated with short electrical
            pulses.
            % https://sci-hub.st/10.1007/BF02344737
            % https://www.ece.uvic.ca/~bctill/papers/neurimp/Dennis_etal_2003.pdf
            An intravaginal drug delivery unit is described in \cite{mcu2},
            which is controlled by a 16-bit Mitsubishi\footnote{Now called
            Renesas} M16C series microcontroller with a \gls{CISC} architecture
            \cite{m16c}.
            It features wireless communication and a gascell for actuation of
            the pump.
            % https://sci-hub.st/10.1016/j.ijpharm.2004.05.023
            A system for biological signal recording is described in
            \cite{mcu1} which features bi-directional wireless data
            transmission.
            The module is implanted on a rabbit to measure electroneurogram
            signals from the sciatic nerve and uses a PIC18F452 (16-bit
            \gls{RISC}) as controller.
            % https://iopscience.iop.org/article/10.1088/0967-3334/26/1/008/pdf
            The work in \cite{pacemaker1} describes software-related energy
            estimation of a pacemaker controlled by a Texas Instruments
            MSP430F1611. 
            The MSP430F16 is a microcontroller with a 16-bit \gls{RISC}
            architecture featuring analog-to-digital and digital-to-analog
            converters \cite{msp430f16x}.
            % https://sci-hub.st/10.1109/ICIINFS.2008.4798432

            An electrocardiograph monitoring \gls{SoC} is described in
            \cite{mcu12}, which contains hardware signal processing blocks and
            a 32-bit ARM Cortex-M0+ (\gls{RISC}) core for the arrhythmia
            detection algorithm.
            % https://sci-hub.st/10.1109/ISSCC.2014.6757494
            \cite{mcu7} implements a Texas Instruments MSP430FR5949 (16-bit
            \gls{RISC} \cite{msp430fr594x}) for data logging of temperature
            measurements in animal brains.
            % http://amygdala.psychdept.arizona.edu/labspace/JclubLabMeetings/Allison-Heller-Implantable+2017.pdf
            A wireless bladder pressure monitor \gls{IMD} is described in
            \cite{mcu9}, which uses a Texas Instruments MSP430FR5994 (16-bit
            \gls{RISC} \cite{msp430fr599x}) and a MicroSemi ZL70123 transceiver
            module.
            % https://sci-hub.st/10.1109/BIOCAS.2017.8325205
            Another MSP430FR5994 is used to run a seizure detection algorithm
            on \gls{EEG} signals based on a \gls{CNN} in \cite{mcu5,mcu8}.
            % https://sci-hub.st/10.1109/IJCNN.2018.8489493
            % https://sci-hub.st/10.1109/EMBC.2018.8512735
            \cite{mcu11} uses two ARM Cortex-M0+ cores for brain-machine
            interfacing; one to run the brain-machine interface algorithms and
            one to execute firmware updates.
            % https://sci-hub.st/%2010.1088/1741-2552/ab4b0c
            % IoT medical platform...
            % https://sci-hub.st/10.1109/TNET.2019.2949805
            \cite{marsman2006dsp} implements a custom 16-bit \gls{DSP} for
            speech comprehension in a cochlear implant.
            % https://sci-hub.st/10.1109/ISCAS.2006.1692671

            Table \ref{tab:proc_imds} shows the referenced publications above
            with their categories, processors, and power consumption.
            \input{tables/proc_imds.tex}
            
            Regarding power consumption and battery life, it is heavily
            dependent on the frequency of use of wireless transmitters and
            actuators.
            When the ratio of the time either of these functions are on is very
            low, the average power consumption is dominated by the power
            consumption of the microcontroller \cite{mcu9}.
            If this is not the case, the use of an \gls{ASIP} would not
            significantly benefit the battery life of the \gls{IMD}.

            The \glspl{IMD} that have space for a battery and \gls{PCB} can use
            a \gls{COTS} processor in a standard package.
            Most works listed use a processor with a \gls{RISC} architecture,
            which is common for \gls{COTS} microcontrollers.
            The \gls{IMD} (\cite{mcu12}) that can not accommodate a \gls{PCB}
            uses wireless charging of a tiny battery and a fabricated \gls{SoC}
            with all functionality, using a Cortex-M0+ core.
            This \gls{IMD} uses a fixed hardware signal-processing front-end,
            which is not programmable, and thus makes the device less flexible.
            An implementation with an \gls{ASIP} with \gls{DSP} functionalities
            would improve the programmability of the device.

            The benchmarks in ImpBench cover most of the workloads for the
            \glspl{IMD} listed in Table~\ref{tab:proc_imds}, except for the
            neural network used in \cite{mcu5,mcu8}.

        \subsection{ASIP}
            \Glspl{ASIP} can be developed by developing an application-specific
            instruction set from the ground up, or by extending an existing
            instruction set with application-specific instructions.
            For the latter method, extending available \gls{RISC} instruction
            sets like RISC-V and MIPS are popular choices, and improvements can
            be measured compared to the original instruction set.
            Depending on the application, the \glspl{ASIP} can achieve
            significant improvements in computational and energy efficiency.
            \cite{peters2005application} and \cite{yassin2009ultra} show that
            algorithms in \glspl{IMD} can also benefit significantly from
            specialised instructions.

            Table \ref{tab:asip_extensions} shows an overview of
            implementations of \glspl{ASIP}, their architecture, and the
            performance gained.
            \input{tables/asip_extensions.tex}

        \subsection{Neural networks in IMDs}\label{sec:nn_in_imds}
            \Glspl{ANN} can be used to extract patterns or trends with less
            computational complexity than traditional algorithms
            \cite{alarifi2018memetic}.
            Table~\ref{tab:nn} shows an overview of publications that employ
            \glspl{ANN} in \glspl{IMD}.
            The table shows that \glspl{ANN} are mostly used in \glspl{IMD} for
            detection or prediction of events.
            Most applications use either a \gls{CNN} or an \gls{RNN}.

            \input{tables/nn.tex}

